(function() {
    var user_service = angular.module('user_service', []);

    user_service.factory('UserService', ['$http', '$q',
        function($http, $q) {

            var REST_SERVICE_URI = 'http://localhost:8080/user/';

            var factory = {
                fetchAllUsers: fetchAllUsers,
                createUser: createUser,
                deleteUser: deleteUser,
				updateUser: updateUser
            };

            return factory;

            function fetchAllUsers() {
                var deferred = $q.defer();
                $http.get(REST_SERVICE_URI + 'all')
                    .then(
                        function(response) {
                            deferred.resolve(response.data);
                        },
                        function(errResponse) {
                            console.error('Error while fetching users');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createUser(user) {
                var deferred = $q.defer();
                $http.post(REST_SERVICE_URI + 'insert/', user)
                    .then(
                        function(response) {
                            deferred.resolve(response.data);
                        },
                        function(errResponse) {
                            console.error('Error while crearing user');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function deleteUser(userId) {
                var deferred = $q.defer();
                $http.delete(REST_SERVICE_URI + 'delete/' + userId)
                    .then(
                        function(response) {
                            deferred.resolve(response.data);
                        },
                        function(errResponse) {
                            console.error('Error while deleting user');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
			
			function updateUser(user, userId) {
				var deferred = $q.defer();
				$http.put(REST_SERVICE_URI + 'update/' + userId, user)
				.then(
				function (response) {
					deferred.resolve(response.data);
				},
				function(errResponse) {
					console.log('Error at updating user', user);
					deferred.reject(errResponse);
				}
				);
				return deferred.promise;
			}


        }
    ]);

})();
