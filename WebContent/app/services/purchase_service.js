'use strict';

(function() {
    var purchase_service = angular.module('purchase_service', []);

    purchase_service.factory('PurchaseService', ['$http', '$q',
        function($http, $q) {

            var REST_SERVICE_URI = 'http://localhost:8080/purchase/';

            var factory = {
                fetchAllPurchases: fetchAllPurchases,
				addPurchase: addPurchase
            };

            return factory;

            function fetchAllPurchases() {
                var deferred = $q.defer();
                $http.get(REST_SERVICE_URI + 'all/' + 65)
                    .then(
                        function(response) {
                            deferred.resolve(response.data);
                        },
                        function(errResponse) {
                            console.error('Error while fetching purchases');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function addPurchase(cartId, itemId, amount) {
                var deferred = $q.defer();
                $http.post(REST_SERVICE_URI + 'add/' + cartId + '/' + itemId + '?amount=' + amount)
                    .then(
                        function(response) {
                            deferred.resolve(response.data);
                        },
                        function(errResponse) {
                            console.error('Error while creating purchase');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }


        }
    ]);

})();
