(function() {
    var login_service = angular.module('login_service', []);

    login_service.factory('LoginService', ['$http', '$location', '$q',
        function($http, $location, $q) {

            var REST_SERVICE_URI = 'http://localhost:8080/user/';

            var factory = {
                login: login
            };

            return factory;

            function login(credentials) {
                var deferred = $q.defer();
                $http.post(REST_SERVICE_URI + 'login/', credentials, {
      headers: {
        'Content-Type': 'application/json'
				}})
                    .then(
                        function(response) {
                            console.log("HEEE", response.data);
							if (response.data === 1)
								$location.url("/users_admin");
							if (response.data === 2)
								$location.url("/items");
                        },
                        function(errResponse) {
                            console.error('Error while crearing credentials');
							alert('Wrong credentials');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
			
	
        }

    ]);

})();
