(function() {
    var itemServices = angular.module('itemServices', []);

    itemServices.factory('ItemFactory', ['$http', 'config',
        function($http, config) {

            var privateItemDetails = function(id) {
                return $http.get(config.API_URL + '/item/details/' + id);
            };

            var privateItemList = function() {
                return $http.get(config.API_URL + '/item/all');
            };
			
			var privateItemReviewList = function(id) {
				return $http.get(config.API_URL + '/item/details/reviews/' + id);
			};

            return {
                findById: function(id) {
                    return privateItemDetails(id);
                },

                findAll: function() {
                    return privateItemList();
                },
				
				findReviews: function(id) {
					return privateItemReviewList(id);
				},
            };
        }
    ]);

})();
