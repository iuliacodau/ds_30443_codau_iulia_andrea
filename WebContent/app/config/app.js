(function() {
    var app = angular.module('angularjs-demo', ['ngRoute', 'configModule',
        'navControllers',
        'userControllers',
        'itemControllers',
        'userServices', 'itemServices',
        'user_service', 'user_controller',
		'login_service', 'login_controller',
		'purchase_service', 'purchase_controller'
    ])
	
	app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);
 
})();
