(function() {

    var user_module = angular.module('user_controller', ['ngRoute'])

    user_module.config(function($routeProvider) {
        $routeProvider.when('/users_admin', {
            templateUrl: 'app/views/user/user_management.html',
            controller: 'user_controller'
            //controllerAs: "allUsersCtrl"
        })

    });


    user_module.controller('user_controller', ['$scope', 'UserService',
        function($scope, UserService) {
            var self = this;
            self.user = {
                userId: null,
                username: '',
                password: '',
                type: '',
                email: '',
                store: ''
            };
            self.users = [];
            self.submit = submit;
            self.edit = edit;
            self.remove = remove;
            self.reset = reset;

            fetchAllUsers();

            function fetchAllUsers() {
                UserService.fetchAllUsers()
                    .then(
                        function(d) {
                            self.users = d;
                        },
                        function(errResponse) {
                            console.error('Error while fetching users');
                        }
                    );
            }

            function createUser(user) {
                UserService.createUser(user)
                    .then(
                        fetchAllUsers,
                        function(errResponse) {
                            console.error('Error while creating user');
                        }
                    );
            }

            function deleteUser(userId) {
                UserService.deleteUser(userId)
                    .then(
                        fetchAllUsers,
                        function(errResponse) {
                            console.error('Error while deleting user');
                        }
                    );
            }
			
			function updateUser(user, userId) {
				UserService.updateUser(user, userId)
				.then(
				fetchAllUsers,
				function(errResponse) {
					console.error('Error while updating user');
				}
				);
			}

            function submit() {
                if (self.user.userId === null) {
                    console.log('Saving new user', self.user);
                    createUser(self.user);
                } else {
					updateUser(self.user, self.user.userId);
                    console.log('Updating user with id', self.user.userId);
                }
                reset();
            }

            function edit(userId) {
                console.log('id to be edited', userId);
                for (var i = 0; i < self.users.length; i++) {
                    if (self.users[i].userId === userId) {
                        self.user = angular.copy(self.users[i]);
                        break;
                    }
                }
            }

            function remove(userId) {
                console.log('id to be deleted', userId);
                if (self.user.userId === userId) {
                    reset();
                }
                deleteUser(userId);
            }

            function reset() {
                self.user = {
                    userId: null,
                    username: '',
                    password: '',
                    type: '',
                    email: '',
                    store: null
                };
                $scope.myForm.$setPristine();
            }
        }
    ]);

})();
