'use strict';

(function() {

    var purchase_module = angular.module('purchase_controller', ['ngRoute'])

    purchase_module.config(function($routeProvider) {
        $routeProvider.when('/purchase/add', {
            templateUrl: 'app/views/item/item-details.html',
            controller: 'purchase_controller'
        })

    });


    purchase_module.controller('purchase_controller', ['$scope', '$routeParams', 'PurchaseService',
        function($scope, $routeParams, PurchaseService) {
			var itemId = $routeParams.id;
            var self = this;
            self.purchasedata = {
                cartId: 67,
				itemId: $routeParams.id,
                amount: 1
            };
			self.purchases=[];
            self.submit = submit;
            self.reset = reset;

            fetchAllPurchases();

            function fetchAllPurchases() {
                PurchaseService.fetchAllPurchases()
                    .then(
                        function(d) {
                            self.purchases = d;
                        },
                        function(errResponse) {
                            console.error('Error while fetching purchases');
                        }
                    );
            }

            function addPurchase(purchasedata) {
                PurchaseService.addPurchase(purchasedata.cartId, purchasedata.itemId, purchasedata.amount)
                    .then(
                        fetchAllPurchases,
						console.log('param', $routeParams.id, purchasedata.amount),
                        function(errResponse) {
                            console.error('Error while creating user');
                        }
                    );
            }


            function submit() {
                if (self.purchasedata !== null) {
                    console.log('Saving new purcase', self.purchasedata, $routeParams.id);
                    addPurchase(self.purchasedata);
                }
                reset();
            }

            function reset() {
            self.purchasedata = {
                cartId: 5,
                itemId: $routeParams.id,
                amount: null
            };
                $scope.myForm.$setPristine();
            }
        }
    ]);

})();
