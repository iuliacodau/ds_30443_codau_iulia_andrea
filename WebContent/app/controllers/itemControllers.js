(function() {

    var itemsModule = angular.module('itemControllers', ['ngRoute'])

    itemsModule.config(function($routeProvider) {
        $routeProvider.when('/items', {
            templateUrl: 'app/views/item/item-list.html',
            controller: 'AllItemsController',
            controllerAs: "allItemsCtrl"
        }).when('/item/:id', {
            templateUrl: 'app/views/item/item-details.html',
            controller: 'ItemController',
            controllerAs: "itemCtrl"
        }).when('/item/reviews/:id', {
            templateUrl: 'app/views/item/item-reviews.html',
            controller: 'ItemReviewsController',
            controllerAs: "itemReviewsCtrl"
        })

    });

    itemsModule.controller('AllItemsController', ['$scope', 'ItemFactory',
        function($scope, ItemFactory) {
            $scope.items = [];
            var promise = ItemFactory.findAll();
            promise.success(function(data) {
                $scope.items = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });

        }
    ]);

    itemsModule.controller('ItemController', ['$scope', '$routeParams',
        'ItemFactory',
        function($scope, $routeParams, ItemFactory) {
            var id = $routeParams.id;
            var promise = ItemFactory.findById(id);
            $scope.item = null;
            promise.success(function(data) {
                $scope.item = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });
        }
    ]);
	
	itemsModule.controller('AllReviewsController', ['$scope', '$routeParams','ItemFactory',
        function($scope, $routeParams,ItemFactory) {
            $scope.reviews = [];
			var id = $routeParams.id;
            var promise = ItemFactory.findReviews(1);
            promise.success(function(data) {
                $scope.reviews = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });

        }
    ]);

    var imageArray = [
        "assets/img/chrono.jpg"
    ];
    var imagePath = "assets/img/after_dark.jpg";

})();
