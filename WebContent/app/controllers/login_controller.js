(function() {

    var login_module = angular.module('login_controller', ['ngRoute'])

    login_module.config(function($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'app/views/user/login.html',
            controller: 'login_controller'
        })

    });


    login_module.controller('login_controller', ['$scope', '$location', 'LoginService',
        function($scope, $location, LoginService) {
            var self = this;
            self.credentials = {
				username: '',
                password: ''
			};
            self.submit = submit;

            function login(credentials) {
                LoginService.login(credentials).
			then(
				function(errResponse) {
                            console.error('Error while creating user in controller');
							alert('Bad Credentials');
                        }
						);
                    
            }

            function submit() {
                if (self.credentials.username !== null) {
                    console.log('Checking credentials', self.credentials);
					login(self.credentials)
                } else {
                    console.log('Update needs to be implemented');
                }
            }

        }
    ]);

})();
