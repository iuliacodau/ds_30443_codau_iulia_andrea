(function() {

    var usersModule = angular.module('userControllers', ['ngRoute'])

    usersModule.config(function($routeProvider) {
        $routeProvider.when('/users', {
            templateUrl: 'app/views/user/user-list.html',
            controller: 'AllUsersController',
            controllerAs: "allUsersCtrl"
        }).when('/user/:id', {
            templateUrl: 'app/views/user/user-details.html',
            controller: 'UserController',
            controllerAs: "userCtrl"
        }).when('/user/delete/:id', {
            templateUrl: 'app/views/user/user-list.html',
            controller: 'UserDeleteController',
            controllerAs: "userDeleteCtrl"
        })

    });

    usersModule.controller('AllUsersController', ['$scope', 'UserFactory',
        function($scope, UserFactory) {
            $scope.users = [];
            var promise = UserFactory.findAll();
            promise.success(function(data) {
                $scope.users = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });

        }
    ]);

    usersModule.controller('UserController', ['$scope', '$routeParams',
        'UserFactory',
        function($scope, $routeParams, UserFactory) {
            var id = $routeParams.id;
            var promise = UserFactory.findById(id);
            $scope.user = null;
            promise.success(function(data) {
                $scope.user = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });
        }
    ]);

    usersModule.controller('UserDeleteController', ['$scope', '$routeParams',
        'UserFactory',
        function($scope, $routeParams, UserFactory) {
            var id = $routeParams.id;
            var promise = UserFactory.deleteById(id);
            $scope.user = null;
            promise.success(function(data) {
                $scope.user = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });
        }
    ]);

    var gems = [{
        id: 1,
        name: 'My name1',
        address: "address 1",
        occupation: 'Something1'
    }, {
        id: 2,
        name: 'My name2',
        address: "address 2",
        occupation: 'Something2'
    }];

})();
