package com.edu.utcn.servlets;

import java.io.IOException;

import com.edu.utcn.entities.DVD;
import com.edu.utcn.producer.Producer;

import javax.servlet.annotation.WebServlet;

/**
 * Created by Asus on 12/23/2016.
 */
@WebServlet(name = "DVDServlet")
public class DVDServlet extends javax.servlet.http.HttpServlet {

    Producer producer;
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        System.out.println("START HERE");
        String title = request.getParameter("title");
        int year = Integer.parseInt(request.getParameter("year"));
        double price = Double.parseDouble(request.getParameter("price"));
        DVD addedDVD = new DVD(title, year, price);
        System.out.println(addedDVD.toString());
        if (addedDVD != null) {
            try {
                producer = new Producer();
                producer.sendDVD(addedDVD);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        response.sendRedirect("index.jsp");
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
