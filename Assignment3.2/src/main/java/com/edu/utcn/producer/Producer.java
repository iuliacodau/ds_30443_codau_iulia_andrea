package com.edu.utcn.producer;

import com.edu.utcn.entities.DVD;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.lang.SerializationUtils;

/**
 * Created by Asus on 12/23/2016.
 */
public class Producer {
    private static final String EXCHANGE_NAME = "logs";

    public void sendDVD(DVD dvd) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        channel.basicPublish(EXCHANGE_NAME, "", null, SerializationUtils.serialize(dvd));
        System.out.println(" [x] Sent '" + dvd.toString() + "'");

        channel.close();
        connection.close();
    }

}
