package com.edu.utcn.consumer;

import com.edu.utcn.entities.DVD;
import com.edu.utcn.services.DVDServiceImpl;
import com.edu.utcn.services.MailServiceImpl;
import com.rabbitmq.client.*;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;

/**
 * Created by Asus on 12/23/2016.
 */
public class ConsumerMails {

    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {

            private MailServiceImpl mailService;

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {

                DVD dvd = (DVD) SerializationUtils.deserialize(body);
                System.out.println(" [x] Received '" + dvd.toString() + "'");
                mailService = new MailServiceImpl("username@gmail.com", "password");
                mailService.sendMail("username@gmail.com",
                        "New DVD was added!", "Hello, a new DVD was just added. Check it out: " + dvd.toString());

            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}
