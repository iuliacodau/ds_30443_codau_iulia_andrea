package com.edu.utcn.services;

/**
 * Created by Asus on 12/23/2016.
 */
public interface MailService {

    public void sendMail(String to, String subject, String content);
}
