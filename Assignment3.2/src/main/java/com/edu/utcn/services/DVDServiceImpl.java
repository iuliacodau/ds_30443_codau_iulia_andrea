package com.edu.utcn.services;

import com.edu.utcn.entities.DVD;

import java.io.*;

/**
 * Created by Asus on 12/23/2016.
 */
public class DVDServiceImpl implements DVDService {

    public void writeToFile(DVD dvd) {
        BufferedWriter output = null;
        try {
            File file = new File(dvd.getTitle() + ".txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write(dvd.toString());
        } catch ( IOException e ) {
            e.printStackTrace();
        } finally {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
