<%@ page import="edu.com.airportManagement.entities.FlightComplex" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 01.11.2016
  Time: 18:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Flights</title>
</head>
<body>

<ul>

    <c:forEach items="${flights}" var="flight">

    <li>Flight Number: ${flight.getFlightNumber()} </li>
    <li>Airplane Type: ${flight.getAirplaneType()} </li>

    <li>Arrival City: ${flight.getArrivalCIty()}  </li>
    <li>Arrival Date: ${flight.getArrivalDate()}  </li>
    <li>Arrival Hour: ${flight.getArrivalHour()}  </li>

    <form method="post" action="/FlightServletUser?latitude=${flight.getArrivalLatitude()}&longitude=${flight.getArrivalLongitude()}&flightNumber=${flight.getFlightNumber()}">
        <input name="time" type="text" value="${flight.getArrivalHour()}" hidden = "true">
        <input type="submit" value="CONVERT TO LOCALTIME">
        <c:if test="${flightNumber!=null}">
            <c:if test="${flightNumber == flight.getFlightNumber}">
        <li>LOCALTIME CONVERSION: <c:out value="${localtime}"/></li>
                </c:if>
        </c:if>
    </form>
    <li>Departure City: ${flight.getDepartureCity()}  </li>
    <li>Departure Date: ${flight.getDepartureDate()}  </li>
    <li>Departure Hour: ${flight.getDepartureHour()}  </li>

<%--    <form method="post" action="/FlightServletUser?latitude=<%=flight.getDepartureLatitude()%>&longitude=<%=flight.getDepartureLongitude()%>&flightNumber=<%=flight.getFlightNumber()%>">
        <input name="time" type="text" value="<%=flight.getDepartureHour()%>" hidden = "true">
        <input type="submit" value="CONVERT TO LOCALTIME">
        <% if (request.getParameter("flightNumber")!=null)
            if (Integer.parseInt(request.getParameter("flightNumber")) == flight.getFlightNumber()) {%>
        <li>LOCALTIME CONVERSION: <%=request.getParameter("localtime")%></li> <%}%>
    </form>--%>

    </c:forEach>
</ul>
<%--
Here will be the part of calculating time zones


<li><a href="/FlightServlet?action=CREATE">CREATE</a> </li>
<li><a href="welcomeUser.jsp">Click here</a> </li>--%>

</body>
</html>
