<%@ page import="edu.com.airportManagement.entities.City" %>
<%@ page import="java.util.List" %>
<%@ page import="edu.com.airportManagement.entities.FlightComplex" %><%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 01.11.2016
  Time: 10:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Create A Flight</title>
</head>
<body>

<form method="post" action="FlightServlet">

    <c:choose>
        <c:when test="${flightToModify!=null}">
            <label>Flight Number:</label>
            <input name="flightNumber" type="text" <c:out value="${flightToModify.getFlightNumber()}" /> readonly="true"/>
            <br>
            <label>Airplane Type:</label>
            <input name="airplaneType" type="text" value="${flightToModify.getAirplaneType()}" required = "true">
            <br>
            <label>Departure City:</label>
            <select name="departureCity">
                <c:forEach items="${cities}" var="city">
                <option value="${city.cityId}">"${city.name}"</option>
                </c:forEach>
            </select>
            <br>
            <label>Departure Date</label>
            <input name="departureDate" type="text" value="${flightToModify.getDepartureDate()}" required = "true">
            <br>
            <label>Departure Hour:</label>
            <input name="departureHour" type="text" value="${flightToModify.getDepartureHour()}" required = "true">
            <br>
            <label>Arrival City:</label>
            <select name="arrivalCity">
                <c:forEach items="${cities}" var="city">
                <option value="${city.getCityId()}">"${city.getName()}"</option>
                </c:forEach>
            </select>
            <br>
            Arrival Date:
            <input name="arrivalDate" type="text" value="${flightToModify.getArrivalDate()}" required = "true">
            <br>
            Arrival Hour:
            <input name="arrivalHour" type="text" value="${flightToModify.getArrivalHour()}" required = "true">
            <br>

        </c:when>
        <c:otherwise>
            Flight Number:
            <input name="flightNumber" type="text" value="" readonly="true"/>
            <br>
            Airplane Type:
            <input name="airplaneType" type="text" value="" required = "true">
            <br>
            Departure City
             <select name="departureCity">
                 <c:forEach items="${cities}" var="city">
                     <option value="${city.cityId}">"${city.name}"</option>
                 </c:forEach>
             </select>
             <br>
             Departure Date
             <input name="departureDate" type="text" value="" required = "true">
             <br>
             Departure Hour
             <input name="departureHour" type="text" value="" required = "true">
             <br>
             Arrival City
             <select name="arrivalCity">
                 <c:forEach items="${cities}" var="city">
                     <option value="${city.getCityId()}">"${city.getName()}"</option>
                 </c:forEach>
             </select>
             <br>
             Arrival Date:
             <input name="arrivalDate" type="text" value="" required = "true">
             <br>
             Arrival Hour:
             <input name="arrivalHour" type="text" value="" required = "true">
             <br>
        </c:otherwise>
    </c:choose>
        <input type="submit" value="CREATE" />

</form>

<footer>
    &copy; 2016 Iulia-Andrea Codau</a>
</footer>

</body>
</html>
