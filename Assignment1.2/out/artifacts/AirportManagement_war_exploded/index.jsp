<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 30.10.2016
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <link rel="stylesheet" href="index.css" type="text/css">
  <head>
    <title>Airport Management - Login</title>
  </head>
  <body>
    <header>Airport Management</header>

    <div>
  <form method="post" action="LoginServlet">
    Username:
    <br>
    <input name="username" type="text" value=""/>
    <br>
    Password:
    <br>
    <input name="password" type="password" value="">
    <br>
    <br>
    <input type="submit" value="Login" />
  </form>
</div>
  <footer>
    &copy; 2016 Iulia-Andrea Codau</a>
  </footer>
  </body>
</html>
