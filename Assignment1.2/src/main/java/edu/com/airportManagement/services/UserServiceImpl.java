package edu.com.airportManagement.services;

import edu.com.airportManagement.daos.UserDAO;
import edu.com.airportManagement.entities.User;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by Iulia on 31.10.2016.
 */
public class UserServiceImpl implements UserService{

    private UserDAO userDAO;

    public UserServiceImpl() {
        userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    @Override
    public User addUser(User user) {
        return userDAO.addUser(user);
    }

    @Override
    public void deleteUser(User user) {
        userDAO.deleteUser(user);
    }

    @Override
    public List<User> findUsers() {
        return userDAO.findUsers();
    }

    @Override
    public User findUser(String username) {
        return userDAO.findUser(username);
    }
}
