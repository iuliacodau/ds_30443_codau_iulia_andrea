package edu.com.airportManagement.services;

import edu.com.airportManagement.daos.FlightDAO;
import edu.com.airportManagement.entities.Flight;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by Iulia on 01.11.2016.
 */
public class FlightServiceImpl implements FlightService {

    private FlightDAO flightDAO;

    public FlightServiceImpl() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
    }

    @Override
    public Flight addFlight(Flight flight) {
        return flightDAO.addFlight(flight);
    }

    @Override
    public List<Flight> findFlights() {
        return flightDAO.findFlights();
    }

    @Override
    public Flight findFlight(int flightNumber) {
        return flightDAO.findFlight(flightNumber);
    }

    @Override
    public void deleteFlight(Flight flight) {
        flightDAO.deleteFlight(flight);
    }

    @Override
    public Flight updateFlight(Flight flight) {
        return flightDAO.updateFlight(flight);
    }
}
