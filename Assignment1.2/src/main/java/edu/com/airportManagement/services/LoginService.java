package edu.com.airportManagement.services;

import edu.com.airportManagement.daos.UserDAO;
import edu.com.airportManagement.entities.User;
import org.hibernate.cfg.Configuration;

/**
 * Created by Iulia on 31.10.2016.
 */
public class LoginService {

    private UserServiceImpl userServiceImpl;

    public boolean authenticate(String username, String password) {
        userServiceImpl = new UserServiceImpl();
        User user = userServiceImpl.findUser(username);
        if (user != null && user.getUsername().equals(username)
                && user.getPassword() != null && user.getPassword().equals(password)) {
            return true;
        } else {
            return false;
        }
    }
}
