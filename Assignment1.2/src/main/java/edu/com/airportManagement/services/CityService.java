package edu.com.airportManagement.services;

import edu.com.airportManagement.entities.City;

import java.util.List;

/**
 * Created by Iulia on 01.11.2016.
 */
public interface CityService {

    public City addCity(City city);
    public City findCity(int cityId);
    public List<City> findCities();
    public void deleteCity(City city);
    public City findCityByName(String name);
}
