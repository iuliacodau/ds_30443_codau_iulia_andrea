package edu.com.airportManagement.services;

import edu.com.airportManagement.entities.User;

import java.util.List;

/**
 * Created by Iulia on 31.10.2016.
 */
public interface UserService {

    public User addUser(User user);
    public void deleteUser(User user);
    public List<User> findUsers();
    public User findUser(String username);
}
