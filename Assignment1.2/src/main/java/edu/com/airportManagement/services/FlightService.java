package edu.com.airportManagement.services;

import edu.com.airportManagement.entities.Flight;

import java.util.List;

/**
 * Created by Iulia on 01.11.2016.
 */
public interface FlightService {

    public Flight addFlight(Flight flight);
    public List<Flight> findFlights();
    public Flight findFlight(int flightNumber);
    public void deleteFlight(Flight flight);
    public Flight updateFlight(Flight flight);
}
