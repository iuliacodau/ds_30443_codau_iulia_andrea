package edu.com.airportManagement.services;

import edu.com.airportManagement.daos.CityDAO;
import edu.com.airportManagement.entities.City;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by Iulia on 01.11.2016.
 */
public class CityServiceImpl implements CityService {

    CityDAO cityDAO;

    public CityServiceImpl() {
        cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    @Override
    public City addCity(City city) {
        return cityDAO.addCity(city);
    }

    @Override
    public City findCity(int cityId) {
        return cityDAO.findCity(cityId);
    }

    @Override
    public List<City> findCities() {
        return cityDAO.findCities();
    }

    @Override
    public void deleteCity(City city) {
        cityDAO.deleteCity(city);
    }

    @Override
    public City findCityByName(String name) {
        return cityDAO.findCityByName(name);
    }
}
