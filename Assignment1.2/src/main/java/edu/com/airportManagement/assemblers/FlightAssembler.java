package edu.com.airportManagement.assemblers;

import edu.com.airportManagement.entities.Flight;
import edu.com.airportManagement.entities.FlightComplex;
import edu.com.airportManagement.services.CityServiceImpl;
import edu.com.airportManagement.services.FlightServiceImpl;

/**
 * Created by Iulia on 01.11.2016.
 */
public class FlightAssembler {

    FlightServiceImpl flightServiceImpl = new FlightServiceImpl();
    CityServiceImpl cityServiceImpl = new CityServiceImpl();

    public Flight flightComplexToSimple(FlightComplex flightComplex) {
        Flight flight = new Flight();
        flight.setFlightNumber(flightComplex.getFlightNumber());
        flight.setAirplaneType(flightComplex.getAirplaneType());
        flight.setArrivalDate(flightComplex.getArrivalDate());
        flight.setArrivalHour(flightComplex.getArrivalHour());
        flight.setDepartureHour(flightComplex.getDepartureHour());
        flight.setDepartureDate(flightComplex.getDepartureDate());
        flight.setArrivalCityId(cityServiceImpl.findCityByName(flightComplex.getArrivalCIty()).getCityId());
        flight.setDepartureCityId(cityServiceImpl.findCityByName(flightComplex.getDepartureCity()).getCityId());
        return flight;
    }

    public FlightComplex flightSimpleToComplex(Flight flight) {
        FlightComplex flightComplex= new FlightComplex();
        flightComplex.setFlightNumber(flight.getFlightNumber());
        flightComplex.setAirplaneType(flight.getAirplaneType());
        flightComplex.setArrivalCIty(cityServiceImpl.findCity(flight.getArrivalCityId()).getName());
        flightComplex.setArrivalLongitude(cityServiceImpl.findCity(flight.getArrivalCityId()).getLongitude());
        flightComplex.setArrivalLatitude(cityServiceImpl.findCity(flight.getArrivalCityId()).getLatitude());
        flightComplex.setArrivalDate(flight.getArrivalDate());
        flightComplex.setArrivalHour(flight.getArrivalHour());
        flightComplex.setDepartureCity(cityServiceImpl.findCity(flight.getDepartureCityId()).getName());
        flightComplex.setDepartureLongitude(cityServiceImpl.findCity(flight.getDepartureCityId()).getLongitude());
        flightComplex.setDepartureLatitude(cityServiceImpl.findCity(flight.getDepartureCityId()).getLatitude());
        flightComplex.setDepartureDate(flight.getDepartureDate());
        flightComplex.setDepartureHour(flight.getDepartureHour());
        return flightComplex;
    }
}
