package edu.com.airportManagement.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Iulia on 30.10.2016.
 */

@Entity
@Table(name = "city")
public class City {

    @Id
    @Column(name = "cityId")
    private int cityId;
    @Column(name = "name")
    private String name;
    @Column(name = "longitude")
    private int longitude;
    @Column(name = "latitude")
    private int latitude;

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "City{" +
                "cityId=" + cityId +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }

}
