package edu.com.airportManagement.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Iulia on 30.10.2016.
 */

@Entity
@Table(name = "flight")
public class Flight {

    @Id
    @Column(name = "flightNumber")
    private int flightNumber;
    @Column(name = "airplaneType")
    private String airplaneType;
    @Column(name = "departureCityId")
    private int departureCityId;
    @Column(name = "arrivalCityId")
    private int arrivalCityId;
    @Column(name = "departureDate")
    private String departureDate;
    @Column(name = "departureHour")
    private String departureHour;
    @Column(name = "arrivalDate")
    private String arrivalDate;
    @Column(name = "arrivalHour")
    private String arrivalHour;

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public int getDepartureCityId() {
        return departureCityId;
    }

    public void setDepartureCityId(int departureCityId) {
        this.departureCityId = departureCityId;
    }

    public int getArrivalCityId() {
        return arrivalCityId;
    }

    public void setArrivalCityId(int arrivalCityId) {
        this.arrivalCityId = arrivalCityId;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureHour() {
        return departureHour;
    }

    public void setDepartureHour(String departureHour) {
        this.departureHour = departureHour;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(String arrivalHour) {
        this.arrivalHour = arrivalHour;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightNumber=" + flightNumber +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCityId=" + departureCityId +
                ", arrivalCityId=" + arrivalCityId +
                ", departureDate='" + departureDate + '\'' +
                ", departureHour='" + departureHour + '\'' +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", arrivalHour='" + arrivalHour + '\'' +
                '}';
    }
}
