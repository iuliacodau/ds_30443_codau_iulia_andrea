package edu.com.airportManagement.entities;

/**
 * Created by Iulia on 01.11.2016.
 */
public class FlightComplex {

    private int flightNumber;
    private String airplaneType;
    private String departureCity;
    private int departureLongitude;
    private int departureLatitude;
    private String arrivalCIty;
    private int arrivalLongitude;
    private int arrivalLatitude;
    private String departureDate;
    private String departureHour;
    private String arrivalDate;
    private String arrivalHour;
    private String localArrivalTime;
    private String localDepartureTime;

    public String getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(String arrivalHour) {
        this.arrivalHour = arrivalHour;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }


    public String getArrivalCIty() {
        return arrivalCIty;
    }

    public void setArrivalCIty(String arrivalCIty) {
        this.arrivalCIty = arrivalCIty;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureHour() {
        return departureHour;
    }

    public void setDepartureHour(String departureHour) {
        this.departureHour = departureHour;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getDepartureLongitude() {
        return departureLongitude;
    }

    public void setDepartureLongitude(int departureLongitude) {
        this.departureLongitude = departureLongitude;
    }

    public int getDepartureLatitude() {
        return departureLatitude;
    }

    public void setDepartureLatitude(int departureLatitude) {
        this.departureLatitude = departureLatitude;
    }

    public int getArrivalLongitude() {
        return arrivalLongitude;
    }

    public void setArrivalLongitude(int arrivalLongitude) {
        this.arrivalLongitude = arrivalLongitude;
    }

    public int getArrivalLatitude() {
        return arrivalLatitude;
    }

    public void setArrivalLatitude(int arrivalLatitude) {
        this.arrivalLatitude = arrivalLatitude;
    }

    public String getLocalArrivalTime() {
        return localArrivalTime;
    }

    public void setLocalArrivalTime(String localArrivalTime) {
        this.localArrivalTime = localArrivalTime;
    }

    public String getLocalDepartureTime() {
        return localDepartureTime;
    }

    public void setLocalDepartureTime(String localDepartureTime) {
        this.localDepartureTime = localDepartureTime;
    }
}
