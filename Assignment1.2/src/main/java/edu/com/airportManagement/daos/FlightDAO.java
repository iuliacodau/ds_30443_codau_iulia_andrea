package edu.com.airportManagement.daos;

import edu.com.airportManagement.entities.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

/**
 * Created by Iulia on 30.10.2016.
 */
public class FlightDAO {

    private static final Log logger = LogFactory.getLog(FlightDAO.class);
    private SessionFactory sessionFactory;

    public FlightDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Flight addFlight(Flight flight) {
        int flightNumber = -1;
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightNumber = (Integer) session.save(flight);
            flight.setFlightNumber(flightNumber);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at adding new flight", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public Flight updateFlight(Flight flight) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at updating flight", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public List<Flight> findFlights() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    public Flight findFlight(int flightNumber) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    public void deleteFlight(Flight flight) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
    }
}
