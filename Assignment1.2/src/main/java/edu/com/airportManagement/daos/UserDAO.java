package edu.com.airportManagement.daos;

import edu.com.airportManagement.util.SessionManager;
import edu.com.airportManagement.entities.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by Iulia on 30.10.2016.
 */
public class UserDAO {

    private static final Log logger = LogFactory.getLog(UserDAO.class);
    private SessionFactory sessionFactory;

    public UserDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public User addUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        System.out.println(user.toString());
        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
            System.out.println("Here");
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at adding new user", e);
        } finally {
            session.close();
        }
        return user;
    }

    public List<User> findUsers() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            users = session.createQuery("FROM User").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return users;
    }

    public User findUser(String username) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    public void deleteUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(user);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
    }

    public static void main(String[] args) {
        SessionManager sm = new SessionManager();
        User user = new User();
        user.setUsername("aaa");
        user.setPassword("password");
        user.setType("admin");
        UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
        userDAO.addUser(user);
    }
}

