package edu.com.airportManagement.servlets;

import edu.com.airportManagement.daos.UserDAO;
import edu.com.airportManagement.entities.User;
import edu.com.airportManagement.services.LoginService;
import edu.com.airportManagement.services.UserService;
import edu.com.airportManagement.services.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Iulia on 31.10.2016.
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {

    private LoginService loginService;
    private User authenticatedUser;
    private UserServiceImpl userServiceImpl;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        userServiceImpl = new UserServiceImpl();
        loginService = new LoginService();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (loginService.authenticate(username, password)) {
            authenticatedUser = userServiceImpl.findUser(username);
            request.getSession().setAttribute("user", authenticatedUser);
            System.out.println(authenticatedUser.getType());
            if ("ADMIN".equals(authenticatedUser.getType())) {
                System.out.println(authenticatedUser.toString());
                response.sendRedirect("welcomeAdmin.jsp");
            } else if ("USER".equals(authenticatedUser.getType())) {
                System.out.println(authenticatedUser.toString());
                response.sendRedirect("welcomeUser.jsp");
            }
        } else {
            response.sendRedirect("index.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
