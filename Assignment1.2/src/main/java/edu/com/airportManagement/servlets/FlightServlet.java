package edu.com.airportManagement.servlets;

import edu.com.airportManagement.assemblers.FlightAssembler;
import edu.com.airportManagement.entities.City;
import edu.com.airportManagement.entities.Flight;
import edu.com.airportManagement.entities.FlightComplex;
import edu.com.airportManagement.entities.User;
import edu.com.airportManagement.services.CityServiceImpl;
import edu.com.airportManagement.services.FlightServiceImpl;
import org.hibernate.id.IntegralDataTypeHolder;
import org.hibernate.sql.Insert;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iulia on 31.10.2016.
 */
@WebServlet(name = "/")
public class FlightServlet extends HttpServlet {

    private FlightServiceImpl flightServiceImpl;
    private CityServiceImpl cityServiceImpl;
    private FlightAssembler flightAssembler;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        flightServiceImpl = new FlightServiceImpl();
        flightAssembler = new FlightAssembler();
        Flight flight = new Flight();
        String flightNumber = request.getParameter("flightNumber");
        flight.setAirplaneType(request.getParameter("airplaneType"));
        flight.setArrivalCityId(Integer.parseInt(request.getParameter("arrivalCity")));
        flight.setArrivalDate(request.getParameter("arrivalDate"));
        flight.setArrivalHour(request.getParameter("arrivalHour"));
        flight.setDepartureCityId(Integer.parseInt(request.getParameter("departureCity")));
        flight.setDepartureDate(request.getParameter("departureDate"));
        flight.setDepartureHour(request.getParameter("departureHour"));
        if (flightNumber == null || flightNumber.isEmpty()) {
            flightServiceImpl.addFlight(flight);
        } else {
            flight.setFlightNumber(Integer.parseInt(flightNumber));
            flightServiceImpl.updateFlight(flight);
        }

        response.sendRedirect("/FlightServlet?action=READ");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userType = ((User)request.getSession().getAttribute("user")).getType();
        if ("ADMIN".equals(userType)) {
            String action = request.getParameter("action");
            flightServiceImpl = new FlightServiceImpl();
            cityServiceImpl = new CityServiceImpl();
            flightAssembler = new FlightAssembler();
            if (action.equals("READ")) {
                List<Flight> listFlights = flightServiceImpl.findFlights();
                List<City> listCities = cityServiceImpl.findCities();
                List<FlightComplex> flightComplexList = new ArrayList<FlightComplex>();
                for (int i = 0; i < listFlights.size(); i++) {
                    FlightComplex flightComplex = new FlightComplex();
                    flightComplex.setFlightNumber(listFlights.get(i).getFlightNumber());
                    flightComplex.setAirplaneType(listFlights.get(i).getAirplaneType());
                    flightComplex.setArrivalCIty(cityServiceImpl.findCity(listFlights.get(i).getArrivalCityId()).getName());
                    flightComplex.setArrivalDate(listFlights.get(i).getArrivalDate());
                    flightComplex.setArrivalHour(listFlights.get(i).getArrivalHour());
                    flightComplex.setDepartureCity(cityServiceImpl.findCity(listFlights.get(i).getDepartureCityId()).getName());
                    flightComplex.setDepartureDate(listFlights.get(i).getDepartureDate());
                    flightComplex.setDepartureHour(listFlights.get(i).getDepartureHour());
                    flightComplexList.add(flightComplex);
                }
                request.setAttribute("flights", flightComplexList);
                RequestDispatcher view = request.getRequestDispatcher("/flight.jsp");
                view.forward(request, response);
            } else if (action.equals("CREATE")) {
                request.setAttribute("cities", cityServiceImpl.findCities());
                RequestDispatcher view = request.getRequestDispatcher("/createFlight.jsp");
                view.forward(request, response);
            }else if (action.equals("UPDATE")) {
                request.setAttribute("cities", cityServiceImpl.findCities());
                int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
                Flight flightToModify = flightServiceImpl.findFlight(flightNumber);
                FlightComplex ftm = flightAssembler.flightSimpleToComplex(flightToModify);
                request.setAttribute("flightToModify", ftm );
                RequestDispatcher view = request.getRequestDispatcher("/createFlight.jsp");
                view.forward(request, response);
            }else if (action.equals("DELETE")) {
                int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
                Flight flightToDelete = flightServiceImpl.findFlight(flightNumber);
                flightServiceImpl.deleteFlight(flightToDelete);
                List<FlightComplex> listOfFlights = new ArrayList<FlightComplex>();
                List<Flight> flights = flightServiceImpl.findFlights();
                for (int i = 0; i < flights.size(); i++) {
                    listOfFlights.add(flightAssembler.flightSimpleToComplex(flights.get(i)));
                }
                request.setAttribute("flights", listOfFlights);
                RequestDispatcher view = request.getRequestDispatcher("/flight.jsp");
                view.forward(request, response);
            }
        } else if ("USER".equals(userType)) {
            RequestDispatcher view = request.getRequestDispatcher("/accessDenied.jsp");
            view.forward(request, response);
        }
    }

}
