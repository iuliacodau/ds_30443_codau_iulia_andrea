package edu.com.airportManagement.servlets;

import edu.com.airportManagement.assemblers.FlightAssembler;
import edu.com.airportManagement.entities.City;
import edu.com.airportManagement.entities.Flight;
import edu.com.airportManagement.entities.FlightComplex;
import edu.com.airportManagement.entities.User;
import edu.com.airportManagement.services.CityServiceImpl;
import edu.com.airportManagement.services.FlightService;
import edu.com.airportManagement.services.FlightServiceImpl;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Iulia on 01.11.2016.
 */
@WebServlet(name = "FlightServletUser")
public class FlightServletUser extends HttpServlet {

    private FlightServiceImpl flightServiceImpl = new FlightServiceImpl();
    private CityServiceImpl cityServiceImpl = new CityServiceImpl();
    private FlightAssembler flightAssembler = new FlightAssembler();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");
        String flightNumber = request.getParameter("flightNumber");
        String time = request.getParameter("time");
        String[] hourMin = time.split(":");
        int hour = Integer.parseInt(hourMin[0]);
        int mins = Integer.parseInt(hourMin[1]);
        int hoursInMins = hour * 60;
        int timeCalculated = hoursInMins + mins;
        URL obj = new URL("http://new.earthtools.org/timezone/"+latitude+"/"+longitude);
        URLConnection conn = obj.openConnection();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = builder.parse(conn.getInputStream());
        } catch (SAXException e) {
            e.printStackTrace();
        }
        double offset = Integer.parseInt(doc.getElementsByTagName("offset").item(0).getTextContent());
        offset=2.0-offset;
        int localtimeInMins = timeCalculated + (int)offset*60;

        int hourNew = localtimeInMins/60;
        localtimeInMins=localtimeInMins-hourNew*60;
        String m = Integer.toString(localtimeInMins);
        String h = Integer.toString(hourNew);
        String localtime=h+":"+m;

        request.setAttribute("localtime", localtime);
        response.sendRedirect("/FlightServletUser?flightNumber="+flightNumber+"&localtime="+localtime);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Here"+request.getParameter("localtime"));
        String flightNumber = request.getParameter("flightNumber");
        String localtime = request.getParameter("localtime");
        String userType = ((User)request.getSession().getAttribute("user")).getType();
        if ("USER".equals(userType)) {
                List<Flight> listFlights = flightServiceImpl.findFlights();
                List<City> listCities = cityServiceImpl.findCities();
                List<FlightComplex> flightComplexList = new ArrayList<FlightComplex>();
                for (int i = 0; i < listFlights.size(); i++) {
                    FlightComplex flightComplex = new FlightComplex();
                    flightComplex.setFlightNumber(listFlights.get(i).getFlightNumber());
                    flightComplex.setAirplaneType(listFlights.get(i).getAirplaneType());
                    flightComplex.setArrivalCIty(cityServiceImpl.findCity(listFlights.get(i).getArrivalCityId()).getName());
                    flightComplex.setArrivalLatitude(cityServiceImpl.findCity(listFlights.get(i).getArrivalCityId()).getLatitude());
                    flightComplex.setArrivalLongitude(cityServiceImpl.findCity(listFlights.get(i).getArrivalCityId()).getLongitude());
                    flightComplex.setArrivalDate(listFlights.get(i).getArrivalDate());
                    flightComplex.setArrivalHour(listFlights.get(i).getArrivalHour());
                    flightComplex.setDepartureCity(cityServiceImpl.findCity(listFlights.get(i).getDepartureCityId()).getName());
                    flightComplex.setDepartureDate(listFlights.get(i).getDepartureDate());
                    flightComplex.setDepartureHour(listFlights.get(i).getDepartureHour());
                    flightComplexList.add(flightComplex);
                }
            request.setAttribute("flights", flightComplexList);
            RequestDispatcher view = request.getRequestDispatcher("/flightUser.jsp");
            view.forward(request, response);
            } else if ("ADMIN".equals(userType)) {
            RequestDispatcher view = request.getRequestDispatcher("/accessDenied.jsp");
            view.forward(request, response);
        }
    }
}
