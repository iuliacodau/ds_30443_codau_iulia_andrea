<%@ page import="edu.com.airportManagement.entities.Flight" %>
<%@ page import="java.util.List" %>
<%@ page import="edu.com.airportManagement.entities.FlightComplex" %>
<%--
  Created by IntelliJ IDEA.
  User: Iulia
  Date: 01.11.2016
  Time: 00:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Flights</title>
</head>
<body>
<table>
    <thead>
    <td>Flight Number</td>
    <td>Airplane Type</td>
    <td>Arrival City</td>
    <td>Arrival Date</td>
    <td>Arrival Hour</td>
    <td>Departure City</td>
    <td>Departure Date</td>
    <td>Departure Hour</td>
    </thead>

    <c:forEach items="${flights}" var="flight">



    <tr>
    <td>Flight Number: ${flight.getFlightNumber()}  </td>
    <td>Airplane Type: ${flight.getAirplaneType()} </td>

    <td>Arrival City: ${flight.getArrivalCIty()} </td>
    <td>Arrival Date: ${flight.getArrivalDate()} </td>
    <td>Arrival Hour: ${flight.getArrivalHour()} </td>

    <td>Departure City: ${flight.getDepartureCity()} </td>
    <td>Departure Date: ${flight.getDepartureDate()} </td>
    <td>Departure Hour: ${flight.getDepartureHour()} </td>

    <td><a href="FlightServlet?action=DELETE&flightNumber=${flight.getFlightNumber()}">DELETE</a> </td>
    <td><a href="FlightServlet?action=UPDATE&flightNumber=${flight.getFlightNumber()}">UPDATE</a> </td>
    </tr>
    </c:forEach>
    </table>

<br>
<br>
<a href="/FlightServlet?action=CREATE">Create new</a>

</body>
</html>
