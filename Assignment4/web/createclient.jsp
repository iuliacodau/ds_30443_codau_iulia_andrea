<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 12/29/2016
  Time: 2:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<link rel="stylesheet" href="index.css">
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<html>
<head>
    <title>Welcome</title>
</head>
<body>

<div class="page-header">
    <h1><b>ULTRA PACK</b><small>  Best package tracking service</small></h1>
</div>

<div class="container">
    <div class="login-container">
        <div class="form-box">
        <legend>New to Ultra Pack? Sign up!</legend>
        <form accept-charset="UTF-8" action="CreateClientServlet" method="post">
            <input name="username" placeholder="Username" type="text">
            <input name="password" placeholder="Password" type="password">
            <input name="passwordRepeat" placeholder="Repeat Password" type="password">
            <p>${message}</p>
            <button class="btn btn-warning" type="submit">Sign up</button>
        </form>
        </div>
    </div>
</div>

</body>
</html>
