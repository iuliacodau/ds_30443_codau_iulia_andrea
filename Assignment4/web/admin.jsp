<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 1/11/2017
  Time: 6:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<link rel="stylesheet" href="index.css">
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<html>
<head>
    <title>Packages</title>
</head>
<body>

<div class="page-header">
    <h1><b>ULTRA PACK</b><small>  Best package tracking service</small></h1>
</div>

<div class="col-md-10 col-md-offset-1">
<table class="table table-hover table-inverse">
    <thead>
    <tr>
        <th>#</th>
        <th>Id</th>
        <th>Sender</th>
        <th>Receiver</th>
        <th>Name</th>
        <th>Description</th>
        <th>Sender-city</th>
        <th>Destination-city</th>
        <th>Tracking</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${packages}" var="item">
        <tr>
            <th scope="row"></th>
            <td>${item.getPackageId()}</td>
            <td>${item.getSender()}</td>
            <td>${item.getReceiver()}</td>
            <td>${item.getName()}</td>
            <td>${item.getDescription()}</td>
            <td>${item.getSenderCityId()}</td>
            <td>${item.getDestinationCityId()}</td>
            <td>${item.isTracking()}</td>
<%--            <c:if test="${item.isTracking()==false}">
                <td><a href="AdminServlet?action=ACTIVATE&packageId=${item.getPackageId()}">Activate tracking</a></td>
            </c:if>--%>
            <c:choose>
                <c:when test="${item.isTracking()==false}">
                    <td><a href="AdminServlet?action=ACTIVATE&packageId=${item.getPackageId()}">Activate tracking</a></td>
                </c:when>
                <c:otherwise>
                    <td><a href="AdminServlet?action=TRACK&packageId=${item.getPackageId()}">Check Tracking</a> </td>
                </c:otherwise>
            </c:choose>
            <td><a href="AdminServlet?action=REMOVE&packageId=${item.getPackageId()}">Delete</a></td>
            <%--<td><a href="AdminServlet?action=TRACK&packageId=${item.getPackageId()}">Check Tracking</a> </td>--%>
        </tr>
    </c:forEach>

    </tbody>
</table>
</div>

<br><br>
<div class="col-md-8 col-md-offset-2">
<h2>Add a New Package</h2>
<small>The tracking will be automatically set to false</small>
</div>

<div class="col-md-8 col-md-offset-2">
    <div class="form-box">
    <form action="AdminServlet" method="post">
        <label>Package Name</label>
        <input name="packageName" type="text">
        <br>
        <label>Sender Name</label>
        <input name="sender" type="text">
        <br>
        <label>Receiver Name</label>
        <input name="receiver" type="text">
        <br>
        <label>Description</label>
        <input name="description" type="text">
        <br>
        <label>Sender City ID</label>
        <input name="senderCityId" type="text">
        <br>
        <label>Destination City ID</label>
        <input name="destinationCityId" type="text">
        <br>
        <br>
        <button type="submit" class="btn btn-primary col-md-2 col-md-offset-5">Add</button>
    </form>
    </div>
</div>

</body>
</html>
