<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 1/10/2017
  Time: 5:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<link rel="stylesheet" href="index.css">
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<html>
<head>
    <title>Packages</title>
</head>
<body>

<div class="page-header">
    <h1><b>ULTRA PACK</b><small>  Best package tracking service</small></h1>
</div>

<table class="table table-hover table-inverse">
    <thead>
    <tr>
        <th>#</th>
        <th>Id</th>
        <th>Sender</th>
        <th>Receiver</th>
        <th>Name</th>
        <th>Description</th>
        <th>Sender-city</th>
        <th>Destination-city</th>
        <th>Tracking</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <th scope="row">1</th>
        <td>${packageSearched.getPackageId()}</td>
        <td>${packageSearched.getSender().getValue()}</td>
        <td>${packageSearched.getReceiver().getValue()}</td>
        <td>${packageSearched.getName().getValue()}</td>
        <td>${packageSearched.getDescription().getValue()}</td>
        <td>${packageSearched.getSenderCityId()}</td>
        <td>${packageSearched.getDestinationCityId()}</td>
        <td>${packageSearched.isTracking()}</td>
        <td><a href="ClientServlet?action=TRACK&packageId=${packageSearched.getPackageId()}">Check Tracking</a> </td>
    </tr>

    </tbody>
</table>

</body>
</html>

