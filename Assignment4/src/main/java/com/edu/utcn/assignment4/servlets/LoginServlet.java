package com.edu.utcn.assignment4.servlets;



import com.edu.utcn.assignment4.services.LoginService;
import com.edu.utcn.assignment4.services.LoginServiceImpl;
import com.mysql.cj.api.Session;
import netUserServicePack.IUserService;
import netUserServicePack.User;


import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Asus on 12/29/2016.
 */

@WebServlet(name = "LoginServlet")
public class LoginServlet extends javax.servlet.http.HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        URL wsdURL = new URL("http://localhost:51511/Services/UserService.svc?singleWsdl");
        QName qName = new QName("http://tempuri.org/", "UserService");
        Service service = Service.create(wsdURL, qName);
        IUserService userService = service.getPort(IUserService.class);

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (userService.findUser(username) != null) {
            User userInDb = userService.findUser(username);
            String usernameInDb = userInDb.getUsername().getValue();
            String passwordInDb = userInDb.getPassword().getValue();
            String userTypeInDb = userInDb.getType().getValue();
            request.getSession().setAttribute("user", usernameInDb);
            if (passwordInDb.equals(password)) {
                if (userTypeInDb.equals("admin"))
                    response.sendRedirect("welcomeAdmin.jsp");
                else if (userTypeInDb.equals("client"))
                    response.sendRedirect("welcomeClient.jsp");
            } else {
                request.setAttribute("message", "Wrong credentials");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
                dispatcher.forward(request,response);
            }
        }
         else {
            request.setAttribute("message", "Wrong credentials");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
            dispatcher.forward(request,response);
        }
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
