package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.entities.Package;
import com.edu.utcn.assignment4.entities.Route;
import com.edu.utcn.assignment4.util.PackageWrapper;
import com.edu.utcn.assignment4.util.RouteWrapper;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by Asus on 1/11/2017.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PackageService {

    @WebMethod
    public Package addPackage(Package pack);

    @WebMethod
    public void deletePacakage(Package pack);

    @WebMethod
    public PackageWrapper findPackages();

    @WebMethod
    public Package findPackage(int packageId);

    @WebMethod
    public Package updatePackage(Package pack);

}
