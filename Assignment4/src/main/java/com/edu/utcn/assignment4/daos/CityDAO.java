package com.edu.utcn.assignment4.daos;


import com.edu.utcn.assignment4.entities.City;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

/**
 * Created by Asus on 12/29/2016.
 */
public class CityDAO {

    private static final Log logger = LogFactory.getLog(CityDAO.class);
    private SessionFactory sessionFactory;

    public CityDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public City addCoty(City city) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at adding new city", e);
        } finally {
            session.close();
        }
        return city;
    }

    public List<City> findCities() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return cities;
    }

    public City findCity(int cityId) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE cityId = :cityId");
            query.setParameter("cityId", cityId);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }

    public void deleteCity(City city) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(city);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
    }
}
