package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.daos.UserDAO;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Created by Asus on 12/29/2016.
 */

@WebService(endpointInterface = "com.edu.utcn.assignment4.services.LoginService")
public class LoginServiceImpl implements LoginService {

    private UserServiceImpl userService;

    public LoginServiceImpl() { }

    //TODO: create method to check wether client or admin
    @WebMethod
    public boolean authenticate(String username, String password) {
        userService = new UserServiceImpl();
        if (userService.findUser(username) != null) {
            if (userService.findUser(username).getPassword().equals(password))
                return true;
        } else {
            System.out.println("No such user");
            return false;
        }
        return false;
    }
}
