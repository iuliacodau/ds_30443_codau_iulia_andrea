package com.edu.utcn.assignment4.daos;

import com.edu.utcn.assignment4.entities.Route;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

/**
 * Created by Asus on 12/29/2016.
 */
public class RouteDAO {

    private static final Log logger = LogFactory.getLog(RouteDAO.class);
    private SessionFactory sessionFactory;

    public RouteDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Route addRoute(Route route) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(route);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at adding new user", e);
        } finally {
            session.close();
        }
        return route;
    }

    public List<Route> findRoutes() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Route> routes = null;
        try {
            tx = session.beginTransaction();
            routes = session.createQuery("FROM Route").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return routes;
    }

    public Route findRoute(int packageId) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Route> routes = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Route WHERE packageId = :packageId");
            query.setParameter("packageId", packageId);
            routes = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return routes != null && !routes.isEmpty() ? routes.get(0) : null;
    }

    public void deleteRoute(Route route) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(route);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
    }
}
