package com.edu.utcn.assignment4.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Asus on 12/29/2016.
 */

@Entity
@Table(name = "city")
public class City {

    @Id
    @Column(name = "cityId")
    private int cityId;

    @Column(name = "name")
    private String name;

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City(int cityId, String name) {
        this.cityId = cityId;
        this.name = name;
    }

    @Override
    public String toString() {
        return "City{" +
                "cityId=" + cityId +
                ", name='" + name + '\'' +
                '}';
    }
}
