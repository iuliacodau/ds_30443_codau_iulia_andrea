package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.daos.UserDAO;
import com.edu.utcn.assignment4.entities.User;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by Asus on 12/29/2016.
 */

@WebService(endpointInterface = "com.edu.utcn.assignment4.services.UserService")
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl() {
        userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    public User addUser(User user) {
        return userDAO.addUser(user);
    }

    public void deleteUser(User user) {
        userDAO.deleteUser(user);
    }

    public User[] findUsers() {
        return userDAO.findUsers();
    }

    public User findUser(String username) {
        return userDAO.findUser(username);
    }
}
