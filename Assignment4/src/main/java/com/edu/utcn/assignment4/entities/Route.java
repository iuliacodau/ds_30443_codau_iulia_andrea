package com.edu.utcn.assignment4.entities;

import javax.persistence.*;

/**
 * Created by Asus on 12/29/2016.
 */

@Entity
@Table(name = "route")
public class Route {

    @Id
    @Column(name = "routeId")
    private int routeId;

    @Column(name = "cityId")
    private int cityId;

    @Column(name = "time")
    private String time;

    @Column(name = "packageId")
    private int packageId;

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public Route() {

    }

    public Route(int routeId, int cityId, String time, int packageId) {
        this.routeId = routeId;
        this.cityId = cityId;
        this.time = time;
        this.packageId = packageId;
    }

    public Route(int cityId, String time, int packageId) {
        this.cityId = cityId;
        this.time = time;
        this.packageId = packageId;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeId=" + routeId +
                ", cityId=" + cityId +
                ", time='" + time + '\'' +
                ", packageId=" + packageId +
                '}';
    }
}
