package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.daos.RouteDAO;
import com.edu.utcn.assignment4.daos.UserDAO;
import com.edu.utcn.assignment4.entities.Route;
import com.edu.utcn.assignment4.util.RouteWrapper;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 1/11/2017.
 */

@WebService(endpointInterface = "com.edu.utcn.assignment4.services.RouteService")
public class RouteServiceImpl implements RouteService {


    private RouteDAO routeDAO;

    public RouteServiceImpl() {
        routeDAO = new RouteDAO(new Configuration().configure().buildSessionFactory());
    }

    public Route addRoute(Route route) {
        return routeDAO.addRoute(route);
    }

    public void deleteRoute(Route route) {
        routeDAO.deleteRoute(route);
    }

    public RouteWrapper findRoutes() {
        List<Route> routeList = routeDAO.findRoutes();
        return new RouteWrapper(routeList);
    }

    public Route findRoute(int routeId) {
        return null;
    }

    public RouteWrapper findRoutesForPackage(int packageId) {
        List<Route> routeList = routeDAO.findRoutes();
        List<Route> routeListForPackage = new ArrayList<Route>();
        for (Route route : routeList) {
            if (route.getPackageId() == packageId)
                routeListForPackage.add(route);
        }
        return new RouteWrapper(routeListForPackage);
    }
}
