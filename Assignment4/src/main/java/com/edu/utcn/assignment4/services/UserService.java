package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.entities.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by Asus on 12/29/2016.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserService {

    @WebMethod
    public User addUser(User user);

    @WebMethod
    public void deleteUser(User user);

    @WebMethod
    public User[] findUsers();

    @WebMethod
    public User findUser(String username);

}
