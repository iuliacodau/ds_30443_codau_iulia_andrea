package com.edu.utcn.assignment4.util;

import com.edu.utcn.assignment4.entities.Package;
import com.edu.utcn.assignment4.entities.Route;
import com.edu.utcn.assignment4.entities.User;
import com.edu.utcn.assignment4.entities.City;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.awt.image.RasterOp;

/**
 * Created by Iulia on 30.10.2016.
 */
public class SessionManager {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {

        try {

            return new Configuration().configure().addAnnotatedClass(User.class)
                    .addAnnotatedClass(City.class)
                    .addAnnotatedClass(Package.class)
                    .addAnnotatedClass(Route.class)
                    .buildSessionFactory();

        } catch (Throwable ex) {

            System.err.println("Initial SessionFactory creation failed." + ex);

            throw new ExceptionInInitializerError(ex);

        }

    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

}
