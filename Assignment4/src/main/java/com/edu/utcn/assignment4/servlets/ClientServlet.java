package com.edu.utcn.assignment4.servlets;

import com.edu.utcn.assignment4.services.RouteService;
import com.edu.utcn.assignment4.services.RouteServiceImpl;
import com.edu.utcn.assignment4.util.RouteWrapper;
import netPackageServicePack.IPackageService;
import netPackageServicePack.Package;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 12/29/2016.
 */
@WebServlet(name = "ClientServlet")
public class ClientServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String packageName = request.getParameter("packageName");
        URL wsdURL = new URL("http://localhost:51511/Services/PackageService.svc?singleWsdl");
        QName qName = new QName("http://tempuri.org/", "PackageService");
        Service service = Service.create(wsdURL, qName);
        IPackageService packageService = service.getPort(IPackageService.class);
        List<com.edu.utcn.assignment4.entities.Package> packageList = new ArrayList<com.edu.utcn.assignment4.entities.Package>();
        Package packageSearched = packageService.searchPackage(packageName);
        if (packageSearched != null) {
            request.setAttribute("packageSearched", packageSearched);
            RequestDispatcher view = request.getRequestDispatcher("/packageSearched.jsp");
            view.forward(request, response);
        } else {
            response.sendRedirect("client.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("type", "client");
        URL wsdURL = new URL("http://localhost:51511/Services/PackageService.svc?singleWsdl");
        QName qName = new QName("http://tempuri.org/", "PackageService");
        Service service = Service.create(wsdURL, qName);
        IPackageService packageService = service.getPort(IPackageService.class);
        List<com.edu.utcn.assignment4.entities.Package> packageList = new ArrayList<com.edu.utcn.assignment4.entities.Package>();
        String username = request.getSession().getAttribute("user").toString();
        String action = request.getParameter("action");
        if(action.equals("READ")) {
            for (int i = 0; i < packageService.findAllPackagesForUser(username).getPackage().size(); i++) {
                netPackageServicePack.Package pack = packageService.findAllPackagesForUser(username).getPackage().get(i);
                packageList.add(new com.edu.utcn.assignment4.entities.Package(
                        pack.getPackageId(),
                        pack.getSender().getValue(),
                        pack.getReceiver().getValue(),
                        pack.getName().getValue(),
                        pack.getDescription().getValue(),
                        pack.getSenderCityId(),
                        pack.getDestinationCityId(),
                        pack.isTracking()
                ));
            }
            request.setAttribute("packages", packageList);
            RequestDispatcher view = request.getRequestDispatcher("/client.jsp");
            view.forward(request, response);
        } else if (action.equals("TRACK")) {
            int packageId = Integer.parseInt(request.getParameter("packageId"));

            URL routesWsdURL = new URL("http://localhost:8080/routesWS?wsdl");
            QName routesQName = new QName("http://services.assignment4.utcn.edu.com/", "RouteServiceImplService");
            Service rService = Service.create(routesWsdURL, routesQName);
            RouteService routeService = rService.getPort(RouteService.class);

            RouteWrapper routeList = routeService.findRoutesForPackage(packageId);
            request.setAttribute("routes", routeList.getAllRoutes());
            RequestDispatcher view = request.getRequestDispatcher("/packageTracker.jsp");
            view.forward(request, response);
        }
    }
}
