package com.edu.utcn.assignment4.servlets;

import netUserServicePack.IUserService;
import netUserServicePack.ObjectFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Asus on 12/29/2016.
 */
@WebServlet(name = "CreateClientServlet")
public class CreateClientServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        URL wsdURL = new URL("http://localhost:51511/Services/UserService.svc?singleWsdl");
        QName qName = new QName("http://tempuri.org/", "UserService");
        Service service = Service.create(wsdURL, qName);
        IUserService userService = service.getPort(IUserService.class);

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String passwordRepeat = request.getParameter("passwordRepeat");




        if (userService.findUser(username) == null){
            if (password.equals(passwordRepeat)) {
                userService.addUser(username, password, "client");
                response.sendRedirect("index.jsp");
            } else {
                request.setAttribute("message", "Repeated password didn't match the initial");
                response.sendRedirect("createclient.jsp");
            }

        } else {
            request.setAttribute("message", "Username already exists");
            response.sendRedirect("createclient.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
