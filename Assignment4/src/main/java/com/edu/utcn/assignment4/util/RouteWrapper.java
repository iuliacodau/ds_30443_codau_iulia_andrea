package com.edu.utcn.assignment4.util;

import com.edu.utcn.assignment4.entities.Route;

import java.util.List;

/**
 * Created by Asus on 1/11/2017.
 */
public class RouteWrapper {

    private List<Route> allRoutes;

    public List<Route> getAllRoutes() {
        return allRoutes;
    }

    public void setAllRoutes(List<Route> allRoutes) {
        this.allRoutes = allRoutes;
    }

    public RouteWrapper(List<Route> allRoutes) {
        this.allRoutes = allRoutes;
    }

    public RouteWrapper() {

    }
}
