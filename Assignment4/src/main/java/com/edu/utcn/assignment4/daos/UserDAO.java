package com.edu.utcn.assignment4.daos;

import com.edu.utcn.assignment4.entities.User;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Asus on 12/29/2016.
 */
public class UserDAO {

    private static final Log logger = LogFactory.getLog(UserDAO.class);
    private SessionFactory sessionFactory;

    public UserDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public User addUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at adding new user", e);
        } finally {
            session.close();
        }
        return user;
    }

    public User[] findUsers() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        User[] users = null;
        List<User> usersFromDB = null;
        try {
            tx = session.beginTransaction();
            usersFromDB = session.createQuery("FROM User").list();
            users = usersFromDB.toArray(users);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return users;
    }

    public User findUser(String username) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<User> usersFromDB = null;
        User[] users = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            usersFromDB = query.list();
            users = usersFromDB.toArray(new User[usersFromDB.size()]);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return users != null && (users.length != 0) ? users[0] : null;
    }

    public void deleteUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(user);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
    }
}
