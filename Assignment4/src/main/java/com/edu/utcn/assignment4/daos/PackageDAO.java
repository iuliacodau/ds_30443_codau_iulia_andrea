package com.edu.utcn.assignment4.daos;

import com.edu.utcn.assignment4.entities.Package;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

/**
 * Created by Asus on 12/29/2016.
 */
public class PackageDAO {

    private static final Log logger = LogFactory.getLog(PackageDAO.class);
    private SessionFactory sessionFactory;

    public PackageDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Package addPacakge(Package mypackage) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(mypackage);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at adding new package", e);
        } finally {
            session.close();
        }
        return mypackage;
    }

    public List<Package> findPackages() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try {
            tx = session.beginTransaction();
            packages = session.createQuery("FROM Package ").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return packages;
    }

    public Package findPackage(int packageId) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE packageId = :packageId");
            query.setParameter("packageId", packageId);
            packages = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
        return packages != null && !packages.isEmpty() ? packages.get(0) : null;
    }

    public Package updatePackage(Package mypackage) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(mypackage);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            logger.error("Error at updating package", e);
        } finally {
            session.close();
        }
        return mypackage;
    }

    public void deletePackage(Package mypackage) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(mypackage);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("", e);
        } finally {
            session.close();
        }
    }
}
