package com.edu.utcn.assignment4.entities;

import javax.persistence.*;

/**
 * Created by Asus on 12/29/2016.
 */

@Entity
@Table(name = "package")
public class Package {

    @Id
    @Column(name = "packageId")
    private int packageId;

    @Column(name = "sender")
    private String sender;

    @Column(name = "receiver")
    private String receiver;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "senderCityId")
    private int senderCityId;

    @Column(name = "destinationCityId")
    private int destinationCityId;

    @Column(name = "tracking")
    private boolean tracking;

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) { this.receiver = receiver; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSenderCityId() {
        return senderCityId;
    }

    public void setSenderCityId(int senderCityId) {
        this.senderCityId = senderCityId;
    }

    public int getDestinationCityId() {
        return destinationCityId;
    }

    public void setDestinationCityId(int destinationCityId) {
        this.destinationCityId = destinationCityId;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public Package() {

    }

    public Package(int packageId, String sender, String receiver, String name, String description, int senderCityId, int destinationCityId, boolean tracking) {
        this.packageId = packageId;
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCityId = senderCityId;
        this.destinationCityId = destinationCityId;
        this.tracking = tracking;
    }

    public Package(String sender, String receiver, String name, String description, int senderCityId, int destinationCityId, boolean tracking) {
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCityId = senderCityId;
        this.destinationCityId = destinationCityId;
        this.tracking = tracking;
    }

    @Override
    public String toString() {
        return "Package{" +
                "packageId=" + packageId +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCityId=" + senderCityId +
                ", destinationCityId=" + destinationCityId +
                ", tracking=" + tracking +
                '}';
    }
}
