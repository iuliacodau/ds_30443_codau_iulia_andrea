package com.edu.utcn.assignment4.publishers;

import com.edu.utcn.assignment4.services.PackageServiceImpl;
import com.edu.utcn.assignment4.services.RouteServiceImpl;
import com.edu.utcn.assignment4.services.UserServiceImpl;

import javax.xml.ws.Endpoint;

/**
 * Created by Asus on 12/31/2016.
 */
public class SOAPPublisher {

    public static void main(String [] args) {
        Endpoint.publish("http://localhost:8080/personWS", new UserServiceImpl());
        Endpoint.publish("http://localhost:8080/routesWS", new RouteServiceImpl());
        Endpoint.publish("http://localhost:8080/packagesWS", new PackageServiceImpl());
    }
}
