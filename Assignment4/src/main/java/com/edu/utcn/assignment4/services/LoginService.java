package com.edu.utcn.assignment4.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by Asus on 12/29/2016.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface LoginService {

    @WebMethod
    boolean authenticate(String username, String password);
}
