package com.edu.utcn.assignment4.servlets;


import com.edu.utcn.assignment4.entities.Route;
import com.edu.utcn.assignment4.services.RouteService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Asus on 1/12/2017.
 */
@WebServlet(name = "TrackingServlet")
public class TrackingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int packageId = Integer.parseInt(request.getParameter("packageId"));
        int cityId = Integer.parseInt(request.getParameter("cityId"));
        String time = request.getParameter("time");

        URL routesWsdURL = new URL("http://localhost:8080/routesWS?wsdl");
        QName routesQName = new QName("http://services.assignment4.utcn.edu.com/", "RouteServiceImplService");
        Service rService = Service.create(routesWsdURL, routesQName);
        RouteService routeService = rService.getPort(RouteService.class);

        routeService.addRoute(new Route(cityId, time, packageId));

        response.sendRedirect("welcomeAdmin.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
