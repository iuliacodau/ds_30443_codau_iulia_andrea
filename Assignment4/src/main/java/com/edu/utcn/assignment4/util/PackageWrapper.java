package com.edu.utcn.assignment4.util;

import com.edu.utcn.assignment4.entities.Package;

import java.util.List;

/**
 * Created by Asus on 1/11/2017.
 */
public class PackageWrapper {

    private List<Package> allPackages;

    public List<Package> getAllPackages() {
        return allPackages;
    }

    public void setAllPackages(List<Package> allPackages) {
        this.allPackages = allPackages;
    }

    public PackageWrapper(List<Package> allPackages) {
        this.allPackages = allPackages;
    }

    public PackageWrapper() {
    }
}
