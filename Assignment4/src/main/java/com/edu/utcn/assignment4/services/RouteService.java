package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.entities.Route;
import com.edu.utcn.assignment4.util.RouteWrapper;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by Asus on 1/11/2017.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface RouteService {

        @WebMethod
        public Route addRoute(Route route);

        @WebMethod
        public void deleteRoute(Route route);

        @WebMethod
        public RouteWrapper findRoutes();

        @WebMethod
        public Route findRoute(int routeId);

        @WebMethod
        public RouteWrapper findRoutesForPackage(int packageId);

}
