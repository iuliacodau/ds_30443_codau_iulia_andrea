package com.edu.utcn.assignment4.services;

import com.edu.utcn.assignment4.daos.PackageDAO;
import com.edu.utcn.assignment4.daos.RouteDAO;
import com.edu.utcn.assignment4.entities.Package;
import com.edu.utcn.assignment4.util.PackageWrapper;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by Asus on 1/11/2017.
 */

@WebService(endpointInterface = "com.edu.utcn.assignment4.services.PackageService")
public class PackageServiceImpl implements PackageService {

    private PackageDAO packageDAO;

    public PackageServiceImpl() {
        packageDAO = new PackageDAO(new Configuration().configure().buildSessionFactory());
    }

    public Package addPackage(Package pack) {
        return packageDAO.addPacakge(pack);
    }

    public void deletePacakage(Package pack) {
        packageDAO.deletePackage(pack);
    }

    public PackageWrapper findPackages() {
        List<Package> packageList = packageDAO.findPackages();
        return new PackageWrapper(packageList);
    }

    public Package findPackage(int packageId) {
        return packageDAO.findPackage(packageId);
    }

    public Package updatePackage(Package pack) {
        return packageDAO.updatePackage(pack);
    }
}
