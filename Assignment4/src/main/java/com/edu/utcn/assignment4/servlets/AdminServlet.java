package com.edu.utcn.assignment4.servlets;

import com.edu.utcn.assignment4.entities.Package;
import com.edu.utcn.assignment4.entities.Route;
import com.edu.utcn.assignment4.services.PackageService;
import com.edu.utcn.assignment4.services.RouteService;
import com.edu.utcn.assignment4.util.PackageWrapper;
import com.edu.utcn.assignment4.util.RouteWrapper;
import netPackageServicePack.IPackageService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus on 12/29/2016.
 */
@WebServlet(name = "AdminServlet")
public class AdminServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String packageName = request.getParameter("packageName");
        String sender =  request.getParameter("sender");
        String receiver = request.getParameter("receiver");
        String description = request.getParameter("description");
        int senderCityId = Integer.parseInt(request.getParameter("senderCityId"));
        int destinationCityId = Integer.parseInt(request.getParameter("destinationCityId"));
        boolean tracking = false;

        URL wsdURL = new URL("http://localhost:8080/packagesWS?wsdl");
        QName qName = new QName("http://services.assignment4.utcn.edu.com/", "PackageServiceImplService");
        Service service = Service.create(wsdURL, qName);
        PackageService packageService = service.getPort(PackageService.class);

        Package addedPackage = new Package(sender, receiver, packageName, description, senderCityId, destinationCityId, tracking);
        packageService.addPackage(addedPackage);

        PackageWrapper packageWrapper = packageService.findPackages();
        List<Package> packageList = packageWrapper.getAllPackages();
        request.setAttribute("packages", packageList);

        RequestDispatcher view = request.getRequestDispatcher("/admin.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("type", "admin");

        URL wsdURL = new URL("http://localhost:8080/packagesWS?wsdl");
        QName qName = new QName("http://services.assignment4.utcn.edu.com/", "PackageServiceImplService");
        Service service = Service.create(wsdURL, qName);
        PackageService packageService = service.getPort(PackageService.class);

        String action = request.getParameter("action");

        if (action != null) {
            if (action.equals("READ")) {
                PackageWrapper packageWrapper = packageService.findPackages();
                List<Package> packageList = packageWrapper.getAllPackages();
                request.setAttribute("packages", packageList);
                RequestDispatcher view = request.getRequestDispatcher("/admin.jsp");
                view.forward(request, response);
            } else if (action.equals("TRACK")) {
                    int packageId = Integer.parseInt(request.getParameter("packageId"));

                    URL routesWsdURL = new URL("http://localhost:8080/routesWS?wsdl");
                    QName routesQName = new QName("http://services.assignment4.utcn.edu.com/", "RouteServiceImplService");
                    Service rService = Service.create(routesWsdURL, routesQName);
                    RouteService routeService = rService.getPort(RouteService.class);

                    RouteWrapper routeList = routeService.findRoutesForPackage(packageId);
                    request.setAttribute("routes", routeList.getAllRoutes());
                    request.setAttribute("packageId", packageId);
                    RequestDispatcher view = request.getRequestDispatcher("/packageTracker.jsp");
                    view.forward(request, response);
            } else if (action.equals("ACTIVATE")) {
                int packageId = Integer.parseInt(request.getParameter("packageId"));
                Package packageToTrack = packageService.findPackage(packageId);
                packageToTrack.setTracking(true);
                packageService.updatePackage(packageToTrack);

                int sender = packageToTrack.getSenderCityId();
                int destination = packageToTrack.getDestinationCityId();

                URL routesWsdURL = new URL("http://localhost:8080/routesWS?wsdl");
                QName routesQName = new QName("http://services.assignment4.utcn.edu.com/", "RouteServiceImplService");
                Service rService = Service.create(routesWsdURL, routesQName);
                RouteService routeService = rService.getPort(RouteService.class);

                Date startDate = new Date();
                Route startRoute = new Route(sender, startDate.toString(), packageId);
                routeService.addRoute(startRoute);

                Date endDate =new Date();
                endDate.setTime(startDate.getTime()+2000000);
                System.out.println(startDate.getTime());
                System.out.println(endDate.getTime());
                Route endRoute = new Route(destination, endDate.toString(), packageId);
                //routeService.addRoute(endRoute);
                RouteWrapper routeList = routeService.findRoutesForPackage(packageId);
                request.setAttribute("routes", routeList.getAllRoutes());
                request.setAttribute("packageId", packageId);
                RequestDispatcher view = request.getRequestDispatcher("/packageTracker.jsp");
                view.forward(request, response);
            }
                else if (action.equals("REMOVE")) {
                int packageId = Integer.parseInt(request.getParameter("packageId"));
                Package deletedPackage = packageService.findPackage(packageId);
                packageService.deletePacakage(deletedPackage);

                PackageWrapper packageWrapper = packageService.findPackages();
                List<Package> packageList = packageWrapper.getAllPackages();
                request.setAttribute("packages", packageList);

                RequestDispatcher view = request.getRequestDispatcher("/admin.jsp");
                view.forward(request, response);
            }
        }
    }
}
