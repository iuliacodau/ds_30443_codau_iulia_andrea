
package netUserServicePack;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="findAllUsersResult" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfuser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findAllUsersResult"
})
@XmlRootElement(name = "findAllUsersResponse")
public class FindAllUsersResponse {

    @XmlElementRef(name = "findAllUsersResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfuser> findAllUsersResult;

    /**
     * Gets the value of the findAllUsersResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfuser }{@code >}
     *     
     */
    public JAXBElement<ArrayOfuser> getFindAllUsersResult() {
        return findAllUsersResult;
    }

    /**
     * Sets the value of the findAllUsersResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfuser }{@code >}
     *     
     */
    public void setFindAllUsersResult(JAXBElement<ArrayOfuser> value) {
        this.findAllUsersResult = value;
    }

}
