
package netUserServicePack;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for package complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="package">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="city" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}city" minOccurs="0"/>
 *         &lt;element name="city1" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}city" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destinationCityId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="packageId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="routes" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfroute" minOccurs="0"/>
 *         &lt;element name="sender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderCityId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tracking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="user" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}user" minOccurs="0"/>
 *         &lt;element name="user1" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}user" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "package", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", propOrder = {
    "city",
    "city1",
    "description",
    "destinationCityId",
    "name",
    "packageId",
    "receiver",
    "routes",
    "sender",
    "senderCityId",
    "tracking",
    "user",
    "user1"
})
public class Package {

    @XmlElementRef(name = "city", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<City> city;
    @XmlElementRef(name = "city1", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<City> city1;
    @XmlElementRef(name = "description", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected Integer destinationCityId;
    @XmlElementRef(name = "name", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    protected Integer packageId;
    @XmlElementRef(name = "receiver", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> receiver;
    @XmlElementRef(name = "routes", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfroute> routes;
    @XmlElementRef(name = "sender", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sender;
    protected Integer senderCityId;
    protected Boolean tracking;
    @XmlElementRef(name = "user", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<User> user;
    @XmlElementRef(name = "user1", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<User> user1;

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link City }{@code >}
     *     
     */
    public JAXBElement<City> getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link City }{@code >}
     *     
     */
    public void setCity(JAXBElement<City> value) {
        this.city = value;
    }

    /**
     * Gets the value of the city1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link City }{@code >}
     *     
     */
    public JAXBElement<City> getCity1() {
        return city1;
    }

    /**
     * Sets the value of the city1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link City }{@code >}
     *     
     */
    public void setCity1(JAXBElement<City> value) {
        this.city1 = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the destinationCityId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDestinationCityId() {
        return destinationCityId;
    }

    /**
     * Sets the value of the destinationCityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDestinationCityId(Integer value) {
        this.destinationCityId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the packageId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPackageId() {
        return packageId;
    }

    /**
     * Sets the value of the packageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPackageId(Integer value) {
        this.packageId = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReceiver(JAXBElement<String> value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the routes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}
     *     
     */
    public JAXBElement<ArrayOfroute> getRoutes() {
        return routes;
    }

    /**
     * Sets the value of the routes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}
     *     
     */
    public void setRoutes(JAXBElement<ArrayOfroute> value) {
        this.routes = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSender(JAXBElement<String> value) {
        this.sender = value;
    }

    /**
     * Gets the value of the senderCityId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSenderCityId() {
        return senderCityId;
    }

    /**
     * Sets the value of the senderCityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSenderCityId(Integer value) {
        this.senderCityId = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTracking(Boolean value) {
        this.tracking = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public JAXBElement<User> getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public void setUser(JAXBElement<User> value) {
        this.user = value;
    }

    /**
     * Gets the value of the user1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public JAXBElement<User> getUser1() {
        return user1;
    }

    /**
     * Sets the value of the user1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public void setUser1(JAXBElement<User> value) {
        this.user1 = value;
    }

}
