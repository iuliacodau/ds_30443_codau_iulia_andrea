
package netUserServicePack;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for city complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="city">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cityId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="packages" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfpackage" minOccurs="0"/>
 *         &lt;element name="packages1" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfpackage" minOccurs="0"/>
 *         &lt;element name="routes" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfroute" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "city", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", propOrder = {
    "cityId",
    "name",
    "packages",
    "packages1",
    "routes"
})
public class City {

    protected Integer cityId;
    @XmlElementRef(name = "name", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "packages", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfpackage> packages;
    @XmlElementRef(name = "packages1", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfpackage> packages1;
    @XmlElementRef(name = "routes", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfroute> routes;

    /**
     * Gets the value of the cityId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * Sets the value of the cityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCityId(Integer value) {
        this.cityId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the packages property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfpackage> getPackages() {
        return packages;
    }

    /**
     * Sets the value of the packages property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public void setPackages(JAXBElement<ArrayOfpackage> value) {
        this.packages = value;
    }

    /**
     * Gets the value of the packages1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfpackage> getPackages1() {
        return packages1;
    }

    /**
     * Sets the value of the packages1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public void setPackages1(JAXBElement<ArrayOfpackage> value) {
        this.packages1 = value;
    }

    /**
     * Gets the value of the routes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}
     *     
     */
    public JAXBElement<ArrayOfroute> getRoutes() {
        return routes;
    }

    /**
     * Sets the value of the routes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}
     *     
     */
    public void setRoutes(JAXBElement<ArrayOfroute> value) {
        this.routes = value;
    }

}
