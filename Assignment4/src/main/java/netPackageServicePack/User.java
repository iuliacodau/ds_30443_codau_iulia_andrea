
package netPackageServicePack;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for user complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="user">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packages" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfpackage" minOccurs="0"/>
 *         &lt;element name="packages1" type="{http://schemas.datacontract.org/2004/07/SOAPService1.Entities}ArrayOfpackage" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", propOrder = {
    "packages",
    "packages1",
    "password",
    "type",
    "username"
})
public class User {

    @XmlElementRef(name = "packages", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfpackage> packages;
    @XmlElementRef(name = "packages1", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfpackage> packages1;
    @XmlElementRef(name = "password", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> password;
    @XmlElementRef(name = "type", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> type;
    @XmlElementRef(name = "username", namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", type = JAXBElement.class, required = false)
    protected JAXBElement<String> username;

    /**
     * Gets the value of the packages property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfpackage> getPackages() {
        return packages;
    }

    /**
     * Sets the value of the packages property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public void setPackages(JAXBElement<ArrayOfpackage> value) {
        this.packages = value;
    }

    /**
     * Gets the value of the packages1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfpackage> getPackages1() {
        return packages1;
    }

    /**
     * Sets the value of the packages1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}
     *     
     */
    public void setPackages1(JAXBElement<ArrayOfpackage> value) {
        this.packages1 = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPassword(JAXBElement<String> value) {
        this.password = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setType(JAXBElement<String> value) {
        this.type = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUsername(JAXBElement<String> value) {
        this.username = value;
    }

}
