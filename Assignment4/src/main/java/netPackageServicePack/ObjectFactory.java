
package netPackageServicePack;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the netPackageServicePack package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _City_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "city");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfroute_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "ArrayOfroute");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _ArrayOfpackage_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "ArrayOfpackage");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Package_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "package");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _Route_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "route");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _User_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "user");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _CityRoutes_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "routes");
    private final static QName _CityPackages_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "packages");
    private final static QName _CityPackages1_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "packages1");
    private final static QName _CityName_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "name");
    private final static QName _FindAllPackagesResponseFindAllPackagesResult_QNAME = new QName("http://tempuri.org/", "findAllPackagesResult");
    private final static QName _UserPassword_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "password");
    private final static QName _UserUsername_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "username");
    private final static QName _UserType_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "type");
    private final static QName _FindAllPackagesForUserUsername_QNAME = new QName("http://tempuri.org/", "username");
    private final static QName _PackageReceiver_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "receiver");
    private final static QName _PackageSender_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "sender");
    private final static QName _PackageCity1_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "city1");
    private final static QName _PackageUser1_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "user1");
    private final static QName _PackageDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "description");
    private final static QName _FindPackageResponseFindPackageResult_QNAME = new QName("http://tempuri.org/", "findPackageResult");
    private final static QName _FindAllPackagesForUserResponseFindAllPackagesForUserResult_QNAME = new QName("http://tempuri.org/", "findAllPackagesForUserResult");
    private final static QName _RouteTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/SOAPService1.Entities", "time");
    private final static QName _SearchPackageName_QNAME = new QName("http://tempuri.org/", "name");
    private final static QName _SearchPackageResponseSearchPackageResult_QNAME = new QName("http://tempuri.org/", "searchPackageResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: netPackageServicePack
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindPackageResponse }
     * 
     */
    public FindPackageResponse createFindPackageResponse() {
        return new FindPackageResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link CheckStatus }
     * 
     */
    public CheckStatus createCheckStatus() {
        return new CheckStatus();
    }

    /**
     * Create an instance of {@link SearchPackage }
     * 
     */
    public SearchPackage createSearchPackage() {
        return new SearchPackage();
    }

    /**
     * Create an instance of {@link FindPackage }
     * 
     */
    public FindPackage createFindPackage() {
        return new FindPackage();
    }

    /**
     * Create an instance of {@link FindAllPackagesResponse }
     * 
     */
    public FindAllPackagesResponse createFindAllPackagesResponse() {
        return new FindAllPackagesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfpackage }
     * 
     */
    public ArrayOfpackage createArrayOfpackage() {
        return new ArrayOfpackage();
    }

    /**
     * Create an instance of {@link FindAllPackagesForUser }
     * 
     */
    public FindAllPackagesForUser createFindAllPackagesForUser() {
        return new FindAllPackagesForUser();
    }

    /**
     * Create an instance of {@link FindAllPackagesForUserResponse }
     * 
     */
    public FindAllPackagesForUserResponse createFindAllPackagesForUserResponse() {
        return new FindAllPackagesForUserResponse();
    }

    /**
     * Create an instance of {@link FindAllPackages }
     * 
     */
    public FindAllPackages createFindAllPackages() {
        return new FindAllPackages();
    }

    /**
     * Create an instance of {@link CheckStatusResponse }
     * 
     */
    public CheckStatusResponse createCheckStatusResponse() {
        return new CheckStatusResponse();
    }

    /**
     * Create an instance of {@link SearchPackageResponse }
     * 
     */
    public SearchPackageResponse createSearchPackageResponse() {
        return new SearchPackageResponse();
    }

    /**
     * Create an instance of {@link ArrayOfroute }
     * 
     */
    public ArrayOfroute createArrayOfroute() {
        return new ArrayOfroute();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link City }
     * 
     */
    public City createCity() {
        return new City();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link City }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "city")
    public JAXBElement<City> createCity(City value) {
        return new JAXBElement<City>(_City_QNAME, City.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "ArrayOfroute")
    public JAXBElement<ArrayOfroute> createArrayOfroute(ArrayOfroute value) {
        return new JAXBElement<ArrayOfroute>(_ArrayOfroute_QNAME, ArrayOfroute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "ArrayOfpackage")
    public JAXBElement<ArrayOfpackage> createArrayOfpackage(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_ArrayOfpackage_QNAME, ArrayOfpackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Package }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "package")
    public JAXBElement<Package> createPackage(Package value) {
        return new JAXBElement<Package>(_Package_QNAME, Package.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Route }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "route")
    public JAXBElement<Route> createRoute(Route value) {
        return new JAXBElement<Route>(_Route_QNAME, Route.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "routes", scope = City.class)
    public JAXBElement<ArrayOfroute> createCityRoutes(ArrayOfroute value) {
        return new JAXBElement<ArrayOfroute>(_CityRoutes_QNAME, ArrayOfroute.class, City.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "packages", scope = City.class)
    public JAXBElement<ArrayOfpackage> createCityPackages(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_CityPackages_QNAME, ArrayOfpackage.class, City.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "packages1", scope = City.class)
    public JAXBElement<ArrayOfpackage> createCityPackages1(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_CityPackages1_QNAME, ArrayOfpackage.class, City.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "name", scope = City.class)
    public JAXBElement<String> createCityName(String value) {
        return new JAXBElement<String>(_CityName_QNAME, String.class, City.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "findAllPackagesResult", scope = FindAllPackagesResponse.class)
    public JAXBElement<ArrayOfpackage> createFindAllPackagesResponseFindAllPackagesResult(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_FindAllPackagesResponseFindAllPackagesResult_QNAME, ArrayOfpackage.class, FindAllPackagesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "password", scope = User.class)
    public JAXBElement<String> createUserPassword(String value) {
        return new JAXBElement<String>(_UserPassword_QNAME, String.class, User.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "username", scope = User.class)
    public JAXBElement<String> createUserUsername(String value) {
        return new JAXBElement<String>(_UserUsername_QNAME, String.class, User.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "packages", scope = User.class)
    public JAXBElement<ArrayOfpackage> createUserPackages(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_CityPackages_QNAME, ArrayOfpackage.class, User.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "type", scope = User.class)
    public JAXBElement<String> createUserType(String value) {
        return new JAXBElement<String>(_UserType_QNAME, String.class, User.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "packages1", scope = User.class)
    public JAXBElement<ArrayOfpackage> createUserPackages1(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_CityPackages1_QNAME, ArrayOfpackage.class, User.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "username", scope = FindAllPackagesForUser.class)
    public JAXBElement<String> createFindAllPackagesForUserUsername(String value) {
        return new JAXBElement<String>(_FindAllPackagesForUserUsername_QNAME, String.class, FindAllPackagesForUser.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "receiver", scope = Package.class)
    public JAXBElement<String> createPackageReceiver(String value) {
        return new JAXBElement<String>(_PackageReceiver_QNAME, String.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link City }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "city", scope = Package.class)
    public JAXBElement<City> createPackageCity(City value) {
        return new JAXBElement<City>(_City_QNAME, City.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "sender", scope = Package.class)
    public JAXBElement<String> createPackageSender(String value) {
        return new JAXBElement<String>(_PackageSender_QNAME, String.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link City }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "city1", scope = Package.class)
    public JAXBElement<City> createPackageCity1(City value) {
        return new JAXBElement<City>(_PackageCity1_QNAME, City.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "user1", scope = Package.class)
    public JAXBElement<User> createPackageUser1(User value) {
        return new JAXBElement<User>(_PackageUser1_QNAME, User.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfroute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "routes", scope = Package.class)
    public JAXBElement<ArrayOfroute> createPackageRoutes(ArrayOfroute value) {
        return new JAXBElement<ArrayOfroute>(_CityRoutes_QNAME, ArrayOfroute.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "user", scope = Package.class)
    public JAXBElement<User> createPackageUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "name", scope = Package.class)
    public JAXBElement<String> createPackageName(String value) {
        return new JAXBElement<String>(_CityName_QNAME, String.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "description", scope = Package.class)
    public JAXBElement<String> createPackageDescription(String value) {
        return new JAXBElement<String>(_PackageDescription_QNAME, String.class, Package.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Package }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "findPackageResult", scope = FindPackageResponse.class)
    public JAXBElement<Package> createFindPackageResponseFindPackageResult(Package value) {
        return new JAXBElement<Package>(_FindPackageResponseFindPackageResult_QNAME, Package.class, FindPackageResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfpackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "findAllPackagesForUserResult", scope = FindAllPackagesForUserResponse.class)
    public JAXBElement<ArrayOfpackage> createFindAllPackagesForUserResponseFindAllPackagesForUserResult(ArrayOfpackage value) {
        return new JAXBElement<ArrayOfpackage>(_FindAllPackagesForUserResponseFindAllPackagesForUserResult_QNAME, ArrayOfpackage.class, FindAllPackagesForUserResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link City }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "city", scope = Route.class)
    public JAXBElement<City> createRouteCity(City value) {
        return new JAXBElement<City>(_City_QNAME, City.class, Route.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Package }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "package", scope = Route.class)
    public JAXBElement<Package> createRoutePackage(Package value) {
        return new JAXBElement<Package>(_Package_QNAME, Package.class, Route.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SOAPService1.Entities", name = "time", scope = Route.class)
    public JAXBElement<String> createRouteTime(String value) {
        return new JAXBElement<String>(_RouteTime_QNAME, String.class, Route.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "name", scope = SearchPackage.class)
    public JAXBElement<String> createSearchPackageName(String value) {
        return new JAXBElement<String>(_SearchPackageName_QNAME, String.class, SearchPackage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Package }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "searchPackageResult", scope = SearchPackageResponse.class)
    public JAXBElement<Package> createSearchPackageResponseSearchPackageResult(Package value) {
        return new JAXBElement<Package>(_SearchPackageResponseSearchPackageResult_QNAME, Package.class, SearchPackageResponse.class, value);
    }

}
