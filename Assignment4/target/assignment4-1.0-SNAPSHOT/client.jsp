<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 1/10/2017
  Time: 3:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<link rel="stylesheet" href="index.css">
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<html>
<head>
    <title>Packages</title>
</head>
<body>

<div class="page-header">
    <h1><b>ULTRA PACK</b><small>  Best package tracking service</small></h1>
</div>

<table class="table table-hover table-inverse">
    <thead>
    <tr>
        <th>#</th>
        <th>Id</th>
        <th>Sender</th>
        <th>Receiver</th>
        <th>Name</th>
        <th>Description</th>
        <th>Sender-city</th>
        <th>Destination-city</th>
        <th>Tracking</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${packages}" var="item">
        <tr>
            <th scope="row">1</th>
            <td>${item.getPackageId()}</td>
            <td>${item.getSender()}</td>
            <td>${item.getReceiver()}</td>
            <td>${item.getName()}</td>
            <td>${item.getDescription()}</td>
            <td>${item.getSenderCityId()}</td>
            <td>${item.getDestinationCityId()}</td>
            <td>${item.isTracking()}</td>
            <c:if test="${item.isTracking()==true}">
                <td><a href="ClientServlet?action=TRACK&packageId=${item.getPackageId()}">Check Tracking</a> </td>
            </c:if>
        </tr>
    </c:forEach>

    </tbody>
</table>

<div class="col-md-8 col-md-offset-2">
<form action="ClientServlet" method="post">
<div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Name</label>
    <div class="col-10">
        <input class="form-control" type="text" name="packageName" id="example-text-input">
    </div>
</div>
<button type="submit" class="btn btn-primary">Search</button>
</form>
</div>

</body>
</html>

