<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 1/11/2017
  Time: 3:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<link rel="stylesheet" href="index.css">
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<html>
<head>
    <title>Packages</title>
</head>
<body>

<div class="page-header">
    <h1><b>ULTRA PACK</b><small>  Best package tracking service</small></h1>
</div>

<h2>Tracking for package: ${packageId}</h2>
<table class="table table-hover table-inverse">
    <thead>
    <tr>
        <th>#</th>
        <th>Route ID</th>
        <th>Time</th>
        <th>City ID</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${routes}" var="item">
        <tr>
            <th scope="row">1</th>
            <td>${item.getRouteId()}</td>
            <td>${item.getTime()}</td>
            <td>${item.getCityId()}</td>
        </tr>
    </c:forEach>

    <c:if test="${type.equals('admin')}">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-box">
                <form action="TrackingServlet" method="post">

                    <input name="packageId" type="hidden" value="${packageId}">
                    <label>City</label>
                    <input name="cityId" type="text">
                    <br>
                    <label>Time</label>
                    <input name="time" type="text">
                    <br><br>
                    <button type="submit" class="btn btn-primary col-md-2 col-md-offset-5">Add</button>
                </form>
            </div>
        </div>
    </c:if>

    </tbody>
</table>

<div class="col-md-2 col-md-offset-5">
    <br><br>
    <a href="AdminServlet?action=READ">Back</a>
    <br><br>
    <a href="http://localhost:8080/">Logout</a>
</div>

</body>
</html>

