<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 12/29/2016
  Time: 2:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<link rel="stylesheet" href="index.css">
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<html>
  <head>
    <title>Welcome</title>
  </head>
  <body>

  <div class="page-header">
    <h1><b>ULTRA PACK</b><small>  Best package tracking service</small></h1>
  </div>

  <div class="container">
    <div class="login-container">
      <div id="output"></div>
      <div class="avatar"></div>
      <div class="form-box">
        <!--INPUT HERE ACTION TO GO TO LOGIN SERVLET-->
        <form action="LoginServlet" method="post">
          <input name="username" type="text" placeholder="username">
          <input name="password" type="password" placeholder="password">
         <%-- <input type="submit" value="Go">--%>
          <p>${message}</p>
          <input class="btn btn-info btn-block login" type="submit" value="GO">
        </form>
        <br>
          <a href="createclient.jsp">REGISTER</a>
      </div>
    </div>
  </div>

  <script>
  $(function(){
  var textfield = $("input[name=user]");
  $('button[type="submit"]').click(function(e) {
  e.preventDefault();
  //little validation just to check username
  if (textfield.val() != "") {
  //$("body").scrollTo("#output");
  $("#output").addClass("alert alert-success animated fadeInUp").html("Welcome back " + "<span style='text-transform:uppercase'>" + textfield.val() + "</span>");
  $("#output").removeClass(' alert-danger');
  $("input").css({
  "height":"0",
  "padding":"0",
  "margin":"0",
  "opacity":"0"
  });
  //change button text
  $('button[type="submit"]').html("continue")
  .removeClass("btn-info")
  .addClass("btn-default").click(function(){
  $("input").css({
  "height":"auto",
  "padding":"10px",
  "opacity":"1"
  }).val("");
  });

  //show avatar
  $(".avatar").css({
  "background-image": "user.png"
  });
  } else {
  //remove success mesage replaced with error message
  $("#output").removeClass(' alert alert-success');
  $("#output").addClass("alert alert-danger animated fadeInUp").html("sorry enter a username ");
  }
  //console.log(textfield.val());

  });
  });
  </script>

  </body>
</html>
