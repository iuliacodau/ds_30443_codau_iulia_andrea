package com.edu.utcn.assignment2.server.start;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.edu.utcn.assignment2.interfaces.computationsService;
import com.edu.utcn.assignment2.server.services.ComputationsServiceImpl;

/**
 * 
 * @author codaui
 *
 */
public class ServerStart {

	private static final int PORT = 1099;
	private static Registry registry;
	
	public static void startRegistry() throws RemoteException {
		registry = LocateRegistry.createRegistry(PORT);
	}
	
	public static void  registerObject(String name, Remote remoteObject) throws RemoteException, AlreadyBoundException, java.rmi.AlreadyBoundException {
		registry.bind(name, remoteObject);
	}
	
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		System.out.println("-----STARTING SERVER...-----");
		startRegistry();
		registerObject(computationsService.class.getSimpleName(), new ComputationsServiceImpl());
	}
}
