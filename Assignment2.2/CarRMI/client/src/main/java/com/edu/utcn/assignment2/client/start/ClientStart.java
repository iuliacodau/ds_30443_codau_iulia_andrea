package com.edu.utcn.assignment2.client.start;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.edu.utcn.assignment2.common.entities.Car;
import com.edu.utcn.assignment2.interfaces.computationsService;

/**
 * 
 * @author codaui
 *
 */
public class ClientStart {

	private static final String HOST = "localhost";
	private static final int PORT = 1099;
	private static Registry registry;

	public static double[] start(Car car) throws RemoteException, NotBoundException {
		registry = LocateRegistry.getRegistry(HOST, PORT);
		computationsService computationsService = (computationsService) registry.lookup(computationsService.class.getSimpleName());
		double[] results = {computationsService.computeTax(car), computationsService.computeSellingPrice(car)};
		return results;
		
		
	}
	
}
