package com.edu.utcn.assignment2.client.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.edu.utcn.assignment2.client.controller.MainController;

/**
 * 
 * @author codaui
 *
 */
public class MainFrame extends JFrame{
	
	private static final long serialVersionUID = -2260604601980205932L;
	
	private final JPanel contentPane;
	private final JLabel carLabel;
	private final JLabel engineSizeLabel;
	private final JLabel yearLabel;
	private final JLabel purchasingPriceLabel;
	private final JTextField engineSizeInput;
	private final JTextField yearInput;
	private final JTextField purchasingPriceInput;
	private final JButton taxButton;
	private final JButton priceButton;
	private final JButton resetButton;
	private final JTextArea taxOutput;
	private final JTextArea priceOutput;
	private final JTextArea infoArea;
	
	public MainFrame(){
		contentPane = new JPanel();
		carLabel = new JLabel("Car");
		engineSizeLabel = new JLabel("Engine Size");
		yearLabel = new JLabel("Year");
		purchasingPriceLabel = new JLabel("Purchasing Price");
		engineSizeInput = new JTextField();
		yearInput = new JTextField();
		purchasingPriceInput = new JTextField();
		taxButton = new JButton("Compute Tax");
		priceButton = new JButton("Compute Selling Price");
		resetButton = new JButton("Reset");
		taxOutput = new JTextArea();
		priceOutput = new JTextArea();
		infoArea = new JTextArea();
		
		contentPane.setLayout(null);
		contentPane.add(carLabel);
		contentPane.add(engineSizeLabel);
		contentPane.add(yearLabel);
		contentPane.add(purchasingPriceLabel);
		contentPane.add(engineSizeInput);
		contentPane.add(yearInput);
		contentPane.add(purchasingPriceInput);
		contentPane.add(taxButton);
		contentPane.add(priceButton);
		contentPane.add(resetButton);
		contentPane.add(taxOutput);
		contentPane.add(priceOutput);
		//contentPane.add(infoArea);
		
		carLabel.setBounds((getWidth() / 2) + 150, getHeight() / 2, 500, 500);
		carLabel.setFont(new Font("Calibri", EXIT_ON_CLOSE, 100));
		carLabel.setSize(200, 100);
		
		engineSizeLabel.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 100, 100, 50);
		engineSizeInput.setBounds((getWidth() / 2) + 200, getHeight() / 2 + 120, 100, 20);
		
		yearLabel.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 140, 100, 50);
		yearInput.setBounds((getWidth() / 2) + 200, getHeight() / 2 + 160, 100, 20);
		
		purchasingPriceLabel.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 180, 100, 50);
		purchasingPriceInput.setBounds((getWidth() / 2) + 200, getHeight() / 2 + 200, 100, 20);
		
		taxButton.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 300, 200, 50);
		priceButton.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 400, 200, 50);
		
		taxOutput.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 500, 200, 50);
		priceOutput.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 600, 200, 50);
		
		infoArea.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 700, 200, 50);
		
		resetButton.setBounds((getWidth() / 2) + 100, getHeight() / 2 + 800, 200, 50);
		
		setContentPane(contentPane);
		
	}
	
	public static void main(String[] args) {
		MainFrame mainFrame = new MainFrame();
		mainFrame.setBackground(Color.BLUE);
		mainFrame.setBounds(500,100,450,900);
		mainFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
		MainController mainController = new MainController(mainFrame);
		mainController.doStuff();
	}

	public JTextField getEngineSizeInput() {
		return engineSizeInput;
	}

	public JTextField getYearInput() {
		return yearInput;
	}

	public JTextField getPurchasingPriceInput() {
		return purchasingPriceInput;
	}

	public JButton getTaxButton() {
		return taxButton;
	}

	public JButton getPriceButton() {
		return priceButton;
	}

	public JButton getResetButton() {
		return resetButton;
	}
	
	public JTextArea getTaxOutput() {
		return taxOutput;
	}

	public JTextArea getPriceOutput() {
		return priceOutput;
	}

	public JTextArea getInfoArea() {
		return infoArea;
	}
	
	
	
	
	


}
