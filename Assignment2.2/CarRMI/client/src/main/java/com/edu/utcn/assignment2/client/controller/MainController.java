package com.edu.utcn.assignment2.client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.edu.utcn.assignment2.client.start.ClientStart;
import com.edu.utcn.assignment2.client.ui.MainFrame;
import com.edu.utcn.assignment2.common.entities.Car;

/**
 * 
 * @author codaui
 *
 */
public class MainController {

	private final MainFrame mainFrame;
	private ActionListener computeTax;
	private ActionListener computePrice;
	private ActionListener reset;
	private Car car;
	
	public MainController(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}
	
	public void doStuff() {
		
		
		computeTax = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				createCarFromInput();
				try {
					double[] results = ClientStart.start(car);
					mainFrame.getTaxOutput().setText(String.valueOf(results[0]));
				} catch (RemoteException e1) {
					mainFrame.getInfoArea().setText("ERROR: Remote call failed");
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					mainFrame.getInfoArea().setText("ERROR: Not bound");
					e1.printStackTrace();
				}
			}
		};
		
		mainFrame.getEngineSizeInput().addActionListener(computeTax);
		mainFrame.getYearInput().addActionListener(computeTax);
		mainFrame.getPurchasingPriceInput().addActionListener(computeTax);
		mainFrame.getTaxButton().addActionListener(computeTax);
		
		computePrice = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				createCarFromInput();
				try {
					double[] results = ClientStart.start(car);
					mainFrame.getPriceOutput().setText(String.valueOf(results[1]));
				} catch (RemoteException e1) {
					mainFrame.getInfoArea().setText("ERROR: Remote call failed");
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					mainFrame.getInfoArea().setText("ERROR: Not bound");
					e1.printStackTrace();
				}
			}
			
		};
		
		reset = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				mainFrame.getInfoArea().setText(null);
				mainFrame.getPriceOutput().setText(null);
				mainFrame.getTaxOutput().setText(null);
				mainFrame.getEngineSizeInput().setText(null);
				mainFrame.getPurchasingPriceInput().setText(null);
				mainFrame.getYearInput().setText(null);
			}
		};
		
		mainFrame.getResetButton().addActionListener(reset);
		
		mainFrame.getEngineSizeInput().addActionListener(computePrice);
		mainFrame.getYearInput().addActionListener(computePrice);
		mainFrame.getPurchasingPriceInput().addActionListener(computePrice);
		mainFrame.getPriceButton().addActionListener(computePrice);
	}
	
	private void createCarFromInput() {
		String engineSize = mainFrame.getEngineSizeInput().getText();
		String year = mainFrame.getYearInput().getText();
		String purchasingPrice = mainFrame.getPurchasingPriceInput().getText();
		car = new Car();
		car.setEngineSize(Integer.parseInt(engineSize));
		car.setYear(Integer.parseInt(year));
		car.setPurchasingPrice(Integer.parseInt(purchasingPrice));
	}
	
	
}
