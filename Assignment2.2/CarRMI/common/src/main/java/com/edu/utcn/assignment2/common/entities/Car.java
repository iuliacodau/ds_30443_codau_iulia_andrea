package com.edu.utcn.assignment2.common.entities;

import java.io.Serializable;

/**
 * 
 * Entity class for Car
 * 
 * @author codaui
 *
 */
public class Car implements Serializable{
	
	private static final long serialVersionUID = 6848501354361092371L;
	private int year;
	private int engineSize;
	private double purchasingPrice;
	
	public Car() {
	}

	public Car(int year, int engineCapacity, double purchasingPrice) {
		super();
		this.year = year;
		this.engineSize = engineCapacity;
		this.purchasingPrice = purchasingPrice;
	}

	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public int getEngineSize() {
		return engineSize;
	}
	
	public void setEngineSize(int engineCapacity) {
		this.engineSize = engineCapacity;
	}
	
	public double getPurchasingPrice() {
		return purchasingPrice;
	}
	
	public void setPurchasingPrice(double purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}
	
	@Override
	public String toString() {
		return "Car [year=" + year + ", engineCapacity=" + engineSize + ", purchasingPrice=" + purchasingPrice
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + engineSize;
		long temp;
		temp = Double.doubleToLongBits(purchasingPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (engineSize != other.engineSize)
			return false;
		if (Double.doubleToLongBits(purchasingPrice) != Double.doubleToLongBits(other.purchasingPrice))
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	
}
