package com.edu.utcn.assignment2.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.edu.utcn.assignment2.common.entities.Car;

/**
 * 
 * 
 * @author codaui
 *
 */
public interface computationsService extends Remote{

	double computeTax(Car car) throws RemoteException;
	
	double computeSellingPrice(Car car) throws RemoteException;
}
