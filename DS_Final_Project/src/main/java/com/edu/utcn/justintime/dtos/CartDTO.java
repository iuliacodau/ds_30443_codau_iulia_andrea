package com.edu.utcn.justintime.dtos;

import com.edu.utcn.justintime.entities.Purchase;
import com.edu.utcn.justintime.entities.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Asus on 2/9/2017.
 */
public class CartDTO {

    private Integer cartId;
    private User user;
    private int total;
    private Set<Purchase> purchases = new HashSet<>(
            0);

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public User getUserId() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Set<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(Set<Purchase> purchases) {
        this.purchases = purchases;
    }

    public CartDTO(){}

    public CartDTO(Integer cartId, User user, int total, Set<Purchase> purchases) {
        super();
        this.cartId = cartId;
        this.user = user;
        this.total = total;
        this.purchases = purchases;
    }

    public static class Builder {

        private Integer nestedcartId;
        private User nesteduser;
        private int nestedtotal;
        private Set<Purchase> nestedpurchases = new HashSet<>(
                0);

        public Builder cartId(Integer id){
            this.nestedcartId = id;
            return this;
        }

        public Builder user(User user){
            this.nesteduser = user;
            return this;
        }

        public Builder total(int total){
            this.nestedtotal = total;
            return this;
        }


        public Builder purchases(Set<Purchase> purchases){
            this.nestedpurchases = purchases;
            return this;
        }

        public CartDTO create(){
            return new CartDTO(nestedcartId,
                    nesteduser,
                    nestedtotal,
                    nestedpurchases);
        }
    }
}
