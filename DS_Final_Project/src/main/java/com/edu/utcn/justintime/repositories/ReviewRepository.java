package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Purchase;
import com.edu.utcn.justintime.entities.Review;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Asus on 2/12/2017.
 */

@Transactional
public interface ReviewRepository extends JpaRepository<Review, Integer> {

    List<Review> findByItem(Item item);
}
