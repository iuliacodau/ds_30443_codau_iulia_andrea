package com.edu.utcn.justintime.dtos;

/**
 * Created by Asus on 2/17/2017.
 */
public class CredentialsWrapper {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CredentialsWrapper(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public CredentialsWrapper(){}
}
