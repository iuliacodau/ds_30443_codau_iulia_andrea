package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.dtos.CredentialsWrapper;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.User;
import com.edu.utcn.justintime.services.CartService;
import com.edu.utcn.justintime.services.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Asus on 2/10/2017.
 */

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CartService cartService;

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public UserDTO getUserById(@PathVariable("id") int id) {
        return userService.findByUserId(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<UserDTO> getAllUsers() {
        return userService.findAll();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertUser(@RequestBody UserDTO userDTO) {
        return userService.create(userDTO);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public int updateUser(@PathVariable("id") int id, @RequestBody UserDTO userDTO) {
        return userService.update(userDTO, id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public int deleteUser(@PathVariable("id") int id) {
        return userService.delete(id);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public int login(@RequestBody CredentialsWrapper credentials) {
        String username = credentials.getUsername();
        String password = credentials.getPassword();
        if (userService.login(username, password).equals("admin") || userService.login(username, password).equals("client")) {
            UserDTO userDTO = userService.findByUsername(username);
            //create a new cart and put it on session when logging in
            CartDTO cartDTO = new CartDTO();
            cartDTO.setUser(new User(userDTO));
            int id = cartService.createEmptyCartForUser(userDTO);
            cartDTO.setCartId(id);

            //request.getSession().setAttribute("cart", cartDTO);
        }
        if (userService.login(username,password).equals("admin"))
            return 1;
        else return 2;
    }
}
