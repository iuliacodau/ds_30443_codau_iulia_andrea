package com.edu.utcn.justintime.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.internet.MimeMessage;

/**
 * Created by Asus on 2/13/2017.
 */

@Service
public class MailService {
    @Autowired
    private MailSender mailSender;

    @Autowired
    private JavaMailSender mimeMailSender;


    @Autowired
    private SimpleMailMessage preConfiguredMessage;

    public void sendMail(final String to, final String subject, final String body) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mimeMailSender.send(new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(
                        mimeMessage, true, "UTF-8");
                messageHelper.setTo(to);
                messageHelper.setSubject(subject);
                messageHelper.setText(body);


            }


        });


    }

    public void sendPreConfiguredMail(String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
}
