package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.services.MailService;
import com.sun.mail.iap.ByteArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Asus on 2/13/2017.
 */

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping("/sendEmail.do")
public class MailController {


    @Autowired
    private MailService mailService;

    @RequestMapping(method = RequestMethod.POST)
    public String doSendEmail(CartDTO cartDTO) {
        // takes input from e-mail form
        String recipientAddress = "cristian.codau@gmail.com";
        String subject = "Your order from Just In Time";
        StringBuilder message = new StringBuilder();
        message.append("Hello " + cartDTO.getUserId().getUsername());
        message.append("Your order details can be accessed at the following link: ");
        mailService.sendMail(recipientAddress, subject, message.toString());

        return "Result";
    }
}
