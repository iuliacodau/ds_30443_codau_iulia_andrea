package com.edu.utcn.justintime.entities;

import com.edu.utcn.justintime.dtos.StoreDTO;

import javax.persistence.*;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table(name = "store")
public class Store {
    private Integer storeId;
    private String city;

    @Id
    @Column(name = "storeId", nullable = false)
    @GeneratedValue
    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    @Column(name = "city", nullable = false, length = 30)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Store store = (Store) o;

        if (storeId != store.storeId) return false;
        if (city != null ? !city.equals(store.city) : store.city != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = storeId;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    public Store(StoreDTO storeDTO) {
        this.storeId = storeDTO.getStoreId();
        this.city = storeDTO.getCity();
    }

    public Store() {

    }

    public Store(Integer storeId, String city) {
        this.storeId = storeId;
        this.city = city;
    }
}
