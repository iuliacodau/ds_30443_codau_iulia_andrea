package com.edu.utcn.justintime.services;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.dtos.StoreDTO;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.Cart;
import com.edu.utcn.justintime.entities.Store;
import com.edu.utcn.justintime.entities.User;
import com.edu.utcn.justintime.repositories.CartRepository;
import com.edu.utcn.justintime.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Asus on 2/13/2017.
 */

@Service
public class CartService {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    UserRepository userRepository;

    public List<CartDTO> findByUserId(int id) {
        User user = userRepository.findByUserId(id);
        List<Cart> carts = cartRepository.findByUser(user);
        List<CartDTO> toReturn = new ArrayList<CartDTO>();
        for (Cart cart : carts) {
            Store store = user.getStoreId();
            StoreDTO storeDTO = new StoreDTO.Builder()
                    .id(store.getStoreId())
                    .city(store.getCity())
                    .create();

            CartDTO dto = new CartDTO.Builder()
                    .cartId(cart.getCartId())
                    .user(cart.getUser())
                    .purchases(cart.getPurchases())
                    .total(cart.getTotal())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public int createEmptyCartForUser(UserDTO userDTO) {
        Cart cart = new Cart();
        cart.setUser(new User(userDTO));
        Cart cartInserted = cartRepository.save(cart);
        return cartInserted.getCartId();
    }
}
