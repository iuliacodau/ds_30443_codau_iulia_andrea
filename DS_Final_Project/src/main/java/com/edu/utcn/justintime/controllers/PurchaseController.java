package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.dtos.ItemDTO;
import com.edu.utcn.justintime.dtos.PurchaseDTO;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.producer.Producer;
import com.edu.utcn.justintime.services.ItemService;
import com.edu.utcn.justintime.services.MailService;
import com.edu.utcn.justintime.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Asus on 2/12/2017.
 */

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/purchase")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    private MailService mailService;

    @Autowired
    private ItemService itemService;

    Producer producer;

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public PurchaseDTO getPurchaseById(@PathVariable("id") int id) {
        return purchaseService.findById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<PurchaseDTO> getAllPurchases() {
        return purchaseService.findAll();
    }

    @RequestMapping(value = "/all/{id}", method = RequestMethod.GET)
    public List<PurchaseDTO> getAllPurchasesByCartId(@PathVariable("id") int id) {
        return purchaseService.findByCartId(id);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertPurchase(@RequestBody PurchaseDTO purchaseDTO) {
        return purchaseService.create(purchaseDTO);
    }

    @RequestMapping(value = "/add/{cartId}/{itemId}", method = RequestMethod.POST)
    public int addNewPurchase(
            @PathVariable("cartId") int cartId,
            @PathVariable("itemId") int itemId,
            @RequestParam(value = "amount") int amount,
            HttpSession session) {
        String recipientAddress = "justintime@gmail.com";
        String subject = "Your order from Just In Time";
        String message = "Hello, you have added a new order to your cart";
        //mailService.sendMail(recipientAddress, subject, message);
        CartDTO cart = (CartDTO)session.getAttribute("cart");
        if (cart == null) {
            cart = new CartDTO();
            cart.setCartId(9);
        }
        ItemDTO itemDTO = itemService.findById(itemId);
        if (itemDTO != null) {
            try {
                producer = new Producer();
                producer.sendItem(new Item(itemDTO));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return purchaseService.addNewPurchase(cart.getCartId(), itemId, amount);
    }
}
