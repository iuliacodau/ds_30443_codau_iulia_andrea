package com.edu.utcn.justintime.entities;

import javax.persistence.*;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table(name = "review")
public class Review {
    private Integer reviewId;
    private Item item;
    private int rating;
    private byte approved;
    private String comment;

    @Id
    @Column(name = "reviewId", nullable = false)
    @GeneratedValue
    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "itemId", nullable = false)
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Column(name = "rating", nullable = false)
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Column(name = "approved", nullable = false)
    public byte getApproved() {
        return approved;
    }

    public void setApproved(byte approved) {
        this.approved = approved;
    }

    @Column(name = "comment", nullable = true, length = 300)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
