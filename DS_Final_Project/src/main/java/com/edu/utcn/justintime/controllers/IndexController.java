package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.dtos.CredentialsWrapper;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.Cart;
import com.edu.utcn.justintime.entities.User;
import com.edu.utcn.justintime.services.CartService;
import com.edu.utcn.justintime.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Asus on 2/12/2017.
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class IndexController {

    @Autowired
    UserService userService;

    @Autowired
    CartService cartService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start() {
            return "Welcome to Just in Time!";
        }

/*    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestParam("username") String username,
                         @RequestParam("password") String password, HttpServletRequest request) {
        if (userService.login(username, password).equals("admin") || userService.login(username, password).equals("client")) {
            UserDTO userDTO = userService.findByUsername(username);
            //create a new cart and put it on session when logging in
            CartDTO cartDTO = new CartDTO();
            cartDTO.setUser(new User(userDTO));
            int id = cartService.createEmptyCartForUser(userDTO);
            cartDTO.setCartId(id);

            request.getSession().setAttribute("cart", cartDTO);
        }
        return userService.login(username,password);
    }*/

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestBody CredentialsWrapper credentials, HttpServletRequest request) {
        String username = credentials.getUsername();
        String password = credentials.getPassword();
        if (userService.login(username, password).equals("admin") || userService.login(username, password).equals("client")) {
            UserDTO userDTO = userService.findByUsername(username);
            //create a new cart and put it on session when logging in
            CartDTO cartDTO = new CartDTO();
            cartDTO.setUser(new User(userDTO));
            int id = cartService.createEmptyCartForUser(userDTO);
            cartDTO.setCartId(id);

            request.getSession().setAttribute("cart", cartDTO);
        }
        return userService.login(username,password);
    }
}
