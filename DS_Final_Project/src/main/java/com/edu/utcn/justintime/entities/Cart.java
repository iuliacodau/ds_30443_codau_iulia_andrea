package com.edu.utcn.justintime.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table (name = "cart")
public class Cart {

    private Integer cartId;

    private User user;

    private int total;

    private Set<Purchase> purchases = new HashSet<Purchase>(
            0);

    @Id
    @Column(name = "cartId", nullable = false)
    @GeneratedValue
    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    @OneToOne(cascade = CascadeType.MERGE)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "total", nullable = false)
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "cart")
    public Set<Purchase> getPurchases() {
        return this.purchases;
    }

    public void setPurchases(Set<Purchase> purchases) {
        this.purchases = purchases;
    }


}
