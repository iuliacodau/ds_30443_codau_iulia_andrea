package com.edu.utcn.justintime.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Created by Asus on 2/13/2017.
 */
@Configuration
public class MailConfig {

    @Autowired
    private Environment env;


    @Bean
    public JavaMailSender javaMailService() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(env.getProperty("smtp.host"));
        javaMailSender.setPort(Integer.parseInt(env.getProperty("smtp.port")));
        javaMailSender.setProtocol(env.getProperty("smtp.protocol"));
        javaMailSender.setUsername(env.getProperty("smtp.username"));
        javaMailSender.setPassword(env.getProperty("smtp.password"));

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        javaMailSender.setJavaMailProperties(props);


        return javaMailSender;
    }

    @Bean
    public SimpleMailMessage simpleMailMessage() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(env.getProperty("smtp.email"));
        simpleMailMessage.setSubject("Activity report");

        return simpleMailMessage;
    }
}