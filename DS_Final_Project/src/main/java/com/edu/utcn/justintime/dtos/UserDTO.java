package com.edu.utcn.justintime.dtos;

import com.edu.utcn.justintime.entities.Store;
import jdk.nashorn.internal.runtime.StoredScript;

/**
 * Created by Asus on 2/10/2017.
 */
public class UserDTO {

    private Integer userId;
    private String type;
    private String username;
    private String password;
    private StoreDTO store;
    private String email;

    public UserDTO() {

    }

    public UserDTO(Integer userId, String type, String username, String password, StoreDTO store, String email) {
        super();
        this.userId = userId;
        this.type = type;
        this.username = username;
        this.password = password;
        this.store = store;
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public StoreDTO getStoreId() {return store;}

    public void setStoreId(StoreDTO store) {
        this.store = store;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static class Builder {
        private Integer nesteduserId;
        private String nestedtype;
        private String nestedusername;
        private String nestedpassword;
        private StoreDTO nestedstore;
        private String nestedemail;


        public Builder id(int id) {
            this.nesteduserId = id;
            return this;
        }

        public Builder type(String type) {
            this.nestedtype = type;
            return this;
        }

        public Builder username(String name) {
            this.nestedusername = name;
            return this;
        }

        public Builder password(String password) {
            this.nestedpassword = password;
            return this;
        }

        public Builder store(StoreDTO store) {
            this.nestedstore = store;
            return this;
        }

        public Builder email(String email) {
            this.nestedemail = email;
            return this;
        }

        public UserDTO create() {
            return new UserDTO(nesteduserId,
                    nestedtype,
                    nestedusername,
                    nestedpassword,
                    nestedstore,
                    nestedemail);
        }
    }
}
