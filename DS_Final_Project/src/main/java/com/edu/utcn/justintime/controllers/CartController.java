package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Asus on 2/13/2017.
 */

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    CartService cartService;

    @RequestMapping(value = "/all/{id}", method = RequestMethod.GET)
    public List<CartDTO> findByUserId(@PathVariable("id") int id) {
        return cartService.findByUserId(id);
    }

}
