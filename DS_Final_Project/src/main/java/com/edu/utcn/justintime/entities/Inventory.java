package com.edu.utcn.justintime.entities;

import com.edu.utcn.justintime.dtos.InventoryDTO;
import com.edu.utcn.justintime.dtos.PurchaseDTO;

import javax.persistence.*;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table(name = "inventory")
public class Inventory {
    private Integer inventoryId;
    private Item item;
    private Store store;
    private int amount;

    @Id
    @Column(name = "inventoryId", nullable = false)
    @GeneratedValue
    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "itemId", nullable = false)
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeId", nullable = false)
    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @Column(name = "amount", nullable = false)
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
