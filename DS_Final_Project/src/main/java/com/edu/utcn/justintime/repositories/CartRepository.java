package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.entities.Cart;
import com.edu.utcn.justintime.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Asus on 2/12/2017.
 */
@Transactional
public interface CartRepository extends JpaRepository<Cart, Integer> {

    List<Cart> findByUser(User user);
}
