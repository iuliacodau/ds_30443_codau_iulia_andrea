package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.dtos.InventoryDTO;
import com.edu.utcn.justintime.entities.Inventory;
import com.edu.utcn.justintime.entities.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

/**
 * Created by Asus on 2/12/2017.
 */

@Transactional
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {

    Inventory findByItem(Item item);

}
