package com.edu.utcn.justintime.dtos;

import com.edu.utcn.justintime.entities.Cart;
import com.edu.utcn.justintime.entities.Item;

/**
 * Created by Asus on 2/9/2017.
 */
public class PurchaseDTO {

    private Integer purchaseId;
    private Cart cart;
    private Item item;
    private int amount;

    public PurchaseDTO(){}

    public PurchaseDTO(Integer purchaseId, int amount) {
        super();
        this.purchaseId = purchaseId;
        this.amount = amount;
    }

    public PurchaseDTO(Integer purchaseId, int amount, Cart cart, Item item) {
        super();
        this.purchaseId = purchaseId;
        this.amount = amount;
        this.cart = cart;
        this.item = item;
    }

    public Integer getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public static class Builder {
        private Integer nestedpurchaseId;
        private int nestedamount;

        public Builder id(Integer id) {
            this.nestedpurchaseId = id;
            return this;
        }

        public Builder amount(int amount) {
            this.nestedamount = amount;
            return this;
        }

        public PurchaseDTO create() {
            return new PurchaseDTO(
                    nestedpurchaseId,
                    nestedamount);
        }
    }
}
