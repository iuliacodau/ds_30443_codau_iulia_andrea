package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.StoreDTO;
import com.edu.utcn.justintime.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Asus on 2/12/2017.
 */

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/store")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public StoreDTO getStoreById(@PathVariable("id") int id) {
        return storeService.findByStoreId(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<StoreDTO> getAllStores() {
        return storeService.findAll();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertStore(@RequestBody StoreDTO storeDTO) {
        return storeService.create(storeDTO);
    }
}
