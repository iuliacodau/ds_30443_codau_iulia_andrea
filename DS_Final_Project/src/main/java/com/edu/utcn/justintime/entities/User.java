package com.edu.utcn.justintime.entities;

import com.edu.utcn.justintime.dtos.UserDTO;

import javax.persistence.*;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table(name = "user")
public class User {
    private Integer userId;
    private String type;
    private String username;
    private String password;
    private Store store;
    private String email;

    @Id
    @Column(name = "userId", nullable = false)
    @GeneratedValue
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "type", nullable = false, length = 10)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "username", nullable = false, length = 30)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", nullable = false, length = 30)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToOne(cascade = CascadeType.MERGE )
    @JoinColumn(name = "storeId")
    public Store getStoreId() {
        return store;
    }

    public void setStoreId(Store store) {
        this.store = store;
    }

    @Column(name = "email", nullable = false, length = 30)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(UserDTO userDTO){
        this.userId = userDTO.getUserId();
        this.type = userDTO.getType();
        this.username = userDTO.getUsername();
        this.password = userDTO.getPassword();
        this.store = new Store(userDTO.getStoreId());
        this.email = userDTO.getEmail();
    }

    public User(){}

}
