package com.edu.utcn.justintime.dtos;

/**
 * Created by Asus on 2/9/2017.
 */
public class StoreDTO {

    private Integer storeId;
    private String city;

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public StoreDTO(){

    }

    public StoreDTO(Integer storeId, String city) {
        super();
        this.storeId = storeId;
        this.city = city;
    }

    public static class Builder{

        private Integer nestedstoreId;
        private String nestedCity;

        public Builder id(Integer id) {
            this.nestedstoreId = id;
            return this;
        }

        public Builder city(String city) {
            this.nestedCity = city;
            return this;
        }

        public StoreDTO create() {
            return new StoreDTO(nestedstoreId, nestedCity);
        }
    }
}
