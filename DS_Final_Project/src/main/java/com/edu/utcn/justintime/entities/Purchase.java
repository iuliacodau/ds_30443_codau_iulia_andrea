package com.edu.utcn.justintime.entities;

import com.edu.utcn.justintime.dtos.PurchaseDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table(name = "purchase")
public class Purchase {
    private Integer purchaseId;
    private Cart cart;
    private Item item;
    private int amount;

    @Id
    @Column(name = "purchaseId", nullable = false)
    @GeneratedValue
    public Integer getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cartId", nullable = false)
    public Cart getCart() {
        return this.cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "itemId", nullable = false)
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Column(name = "amount", nullable = false)
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Purchase(PurchaseDTO purchaseDTO) {
        this.purchaseId = purchaseDTO.getPurchaseId();
        this.cart = purchaseDTO.getCart();
        this.item = purchaseDTO.getItem();
        this.amount = purchaseDTO.getAmount();
    }

    public Purchase(){}

}
