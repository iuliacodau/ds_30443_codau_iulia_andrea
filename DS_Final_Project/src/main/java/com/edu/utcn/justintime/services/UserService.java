package com.edu.utcn.justintime.services;

import com.edu.utcn.justintime.dtos.StoreDTO;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.Review;
import com.edu.utcn.justintime.entities.Store;
import com.edu.utcn.justintime.entities.User;
import com.edu.utcn.justintime.errorhandlers.EntityValidationException;
import com.edu.utcn.justintime.errorhandlers.ResourceNotFoundException;
import com.edu.utcn.justintime.repositories.ReviewRepository;
import com.edu.utcn.justintime.repositories.StoreRepository;
import com.edu.utcn.justintime.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Asus on 2/10/2017.
 */
@Service
public class UserService {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);

    @Autowired
    UserRepository userRepository;

    @Autowired
    StoreRepository storeRepository;

    public UserDTO findByUserId(int id) {
        User user = userRepository.findByUserId(id);
        if (user == null) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }

        Store store = user.getStoreId();
        StoreDTO storeDTO = new StoreDTO.Builder()
                .id(store.getStoreId())
                .city(store.getCity())
                .create();

        UserDTO dto = new UserDTO.Builder()
                .id(user.getUserId())
                .type(user.getType())
                .username(user.getUsername())
                .password(user.getPassword())
                .store(storeDTO)
                .email(user.getEmail())
                .create();
        return dto;
    }

    public UserDTO findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }

        Store store = user.getStoreId();
        StoreDTO storeDTO = new StoreDTO.Builder()
                .id(store.getStoreId())
                .city(store.getCity())
                .create();

        UserDTO dto = new UserDTO.Builder()
                .id(user.getUserId())
                .type(user.getType())
                .username(user.getUsername())
                .password(user.getPassword())
                .store(storeDTO)
                .email(user.getEmail())
                .create();
        return dto;
    }

    public int delete(int id) {
        if (userRepository.findByUserId(id) != null )
            userRepository.delete(id);
        return id;
    }

    public List<UserDTO> findAll() {
        List<User> users = userRepository.findAll();
        List<UserDTO> toReturn = new ArrayList<UserDTO>();
        for (User user : users) {
            Store store = user.getStoreId();
            StoreDTO storeDTO = new StoreDTO.Builder()
                    .id(store.getStoreId())
                    .city(store.getCity())
                    .create();

            UserDTO dto = new UserDTO.Builder()
                    .id(user.getUserId())
                    .type(user.getType())
                    .username(user.getUsername())
                    .password(user.getPassword())
                    .store(storeDTO)
                    .email(user.getEmail())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public String login(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user.getPassword().equals(password)) {
                return user.getType();
        }
        else return "wrong";
    }

    public int create(UserDTO userDTO) {
        List<String> validationErrors = validateUser(userDTO);
        if (!validationErrors.isEmpty()) {
            throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
        }

        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setType(userDTO.getType());
        if (user.getType().equals("client")) {
            Store store = storeRepository.findByStoreId(1);
            user.setStoreId(store);
        }
        else {
            Store store = storeRepository.findByStoreId(2);
            user.setStoreId(store);
        }
        User userInserted = userRepository.save(user);
        return userInserted.getUserId();
    }

    private List<String> validateUser(UserDTO user) {
        List<String> validationErrors = new ArrayList<String>();

        if (user.getUsername() == null || "".equals(user.getUsername())) {
            validationErrors.add("Username field should not be empty");
        }

        if (user.getPassword() == null || "".equals(user.getPassword())) {
            validationErrors.add("Password field should not be empty");
        }

        if (user.getEmail() == null || !validateEmail(user.getEmail())) {
            validationErrors.add("Email does not have the correct format.");
        }

        return validationErrors;
    }

    public static boolean validateEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public int update(UserDTO userDTO, Integer id) {
        userRepository.findOne(id);
       // Store store = storeRepository.findOne(userDTO.getStoreId().getStoreId());
        Store store = new Store();
        if (userDTO.getType().equals("client"))
            store = storeRepository.findOne(1);
        if (userDTO.getType().equals("admin"))
            store = storeRepository.findOne(2);
        User user = new User();
        user.setUserId(id);
        user.setStoreId(store);
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setType(userDTO.getType());
        user.setEmail(userDTO.getEmail());
        return userRepository.save(user).getUserId();
    }
}
