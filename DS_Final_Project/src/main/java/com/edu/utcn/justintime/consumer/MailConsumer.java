package com.edu.utcn.justintime.consumer;

import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.services.MailServiceImpl;
import com.rabbitmq.client.*;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;

/**
 * Created by Asus on 2/17/2017.
 */
public class MailConsumer {


    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {

            private MailServiceImpl mailService;

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {

                Item item = (Item) SerializationUtils.deserialize(body);
                System.out.println(" [x] Received '" + item.getName() + "'");
                mailService = new MailServiceImpl("cristian.codau@gmail.com", "crianiul");
                mailService.sendMail("cristian.codau@gmail.com",
                        "New Order!", "Hello, a new item was just added in the cart. Check it out: " + item.getName() + " with the total price " + item.getPrice());

            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}
