package com.edu.utcn.justintime.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Asus on 2/10/2017.
 */
@Configuration
@ComponentScan(basePackages = "com.edu.utcn.justintime")
public class AppConfig {
}
