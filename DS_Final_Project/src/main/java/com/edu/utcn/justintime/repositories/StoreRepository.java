package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.entities.Store;
import com.edu.utcn.justintime.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

/**
 * Created by Asus on 2/12/2017.
 */

@Transactional
public interface StoreRepository extends JpaRepository<Store, Integer> {

    Store findByStoreId(Integer id);
}
