package com.edu.utcn.justintime.services;

import com.edu.utcn.justintime.dtos.StoreDTO;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.Store;
import com.edu.utcn.justintime.entities.User;
import com.edu.utcn.justintime.errorhandlers.ResourceNotFoundException;
import com.edu.utcn.justintime.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Created by Asus on 2/12/2017.
 */

@Service
public class StoreService {

    @Autowired
    StoreRepository storeRepository;

    public StoreDTO findByStoreId(int id) {
        Store store = storeRepository.findByStoreId(id);
        if (store == null) {
            throw new ResourceNotFoundException(Store.class.getSimpleName());
        }

        StoreDTO dto = new StoreDTO.Builder()
                .id(store.getStoreId())
                .city(store.getCity())
                .create();

        return dto;
    }

    public List<StoreDTO> findAll() {
        List<Store> stores = storeRepository.findAll();
        List<StoreDTO> toReturn = new ArrayList<StoreDTO>();
        for (Store store : stores) {
            StoreDTO dto = new StoreDTO.Builder()
                    .id(store.getStoreId())
                    .city(store.getCity())
                    .create();

            toReturn.add(dto);
        }
        return toReturn;
    }

    public int create(StoreDTO storeDTO) {

        Store store = new Store(storeDTO);
        store = storeRepository.save(store);
        return store.getStoreId();
    }
}
