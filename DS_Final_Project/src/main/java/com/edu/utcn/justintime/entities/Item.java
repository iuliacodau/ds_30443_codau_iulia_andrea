package com.edu.utcn.justintime.entities;

import com.edu.utcn.justintime.dtos.ItemDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Asus on 1/27/2017.
 */
@Entity
@Table(name = "item")
public class Item implements Serializable {
    private Integer itemId;
    private String name;
    private int price;
    private String brand;
    private Set<Purchase> purchases = new HashSet<Purchase>(
            0);
    private Set<Inventory> inventories = new HashSet<Inventory>(
            0);
    private Set<Review> reviews = new HashSet<Review>(
            0);

    @Id
    @Column(name = "itemId", nullable = false)
    @GeneratedValue
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @Column(name = "name", nullable = false, length = 10)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "price", nullable = false)
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Column(name = "brand", nullable = true, length = 50)
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
    public Set<Purchase> getPurchases() {
        return this.purchases;
    }

    public void setPurchases(Set<Purchase> purchases) {
        this.purchases = purchases;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
    public Set<Inventory> getInventories() {
        return this.inventories;
    }

    public void setInventories(Set<Inventory> inventories) {
        this.inventories = inventories;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
    public Set<Review> getReviews() {
        return this.reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public Item(ItemDTO itemDTO) {
        this.itemId = itemDTO.getItemId();
        this.name = itemDTO.getName();
        this.price = itemDTO.getPrice();
        this.brand = itemDTO.getBrand();
    }

    public Item(){};

}
