package com.edu.utcn.justintime.services;

import com.edu.utcn.justintime.dtos.CartDTO;
import com.edu.utcn.justintime.dtos.ItemDTO;
import com.edu.utcn.justintime.dtos.PurchaseDTO;
import com.edu.utcn.justintime.entities.Cart;
import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Purchase;
import com.edu.utcn.justintime.entities.Review;
import com.edu.utcn.justintime.repositories.ItemRepository;
import com.edu.utcn.justintime.repositories.PurchaseRepository;
import com.edu.utcn.justintime.repositories.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;

/**
 * Created by Asus on 2/9/2017.
 */

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ReviewRepository reviewRepository;

    public ItemDTO findById(int id) {
        Item item = itemRepository.findByItemId(id);
        if (item == null) {
            System.out.println("Not good");
        }
        ItemDTO dto = new ItemDTO.Builder()
                .id(item.getItemId())
                .name(item.getName())
                .price(item.getPrice())
                .brand(item.getBrand())
                .create();
        return dto;
    }

    public List<ItemDTO> findAll() {
        List<Item> items = itemRepository.findAll();
        List<ItemDTO> toReturn = new ArrayList<ItemDTO>();
        for (Item item : items) {
            ItemDTO dto = new ItemDTO.Builder()
                    .id(item.getItemId())
                    .name(item.getName())
                    .price(item.getPrice())
                    .brand(item.getBrand())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public List<Review> getAllReviewsForItem(int id) {
        Item item = itemRepository.findOne(id);
        List<Review> reviews = new ArrayList<>();
        reviews = reviewRepository.findByItem(item);
        return reviews;
    }

}
