package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

/**
 * Created by Asus on 2/9/2017.
 */

@Transactional
public interface ItemRepository extends JpaRepository<Item, Integer> {

    Item findByItemId(Integer id);
}
