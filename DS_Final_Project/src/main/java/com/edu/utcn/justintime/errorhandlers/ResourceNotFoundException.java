package com.edu.utcn.justintime.errorhandlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Asus on 2/12/2017.
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Entity was not found in database.")
public class ResourceNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String msg){
            super(msg);
        }

}
