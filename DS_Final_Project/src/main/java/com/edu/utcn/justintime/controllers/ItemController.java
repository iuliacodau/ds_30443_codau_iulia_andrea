package com.edu.utcn.justintime.controllers;

import com.edu.utcn.justintime.dtos.ItemDTO;
import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.Review;
import com.edu.utcn.justintime.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Asus on 2/9/2017.
 */

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public ItemDTO getItemById(@PathVariable("id") int id) {
        return itemService.findById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ItemDTO> getAllItems() {
        return itemService.findAll();
    }

    @RequestMapping(value = "/details/reviews/{id}", method = RequestMethod.GET)
    public List<Review> getReviewsById(@PathVariable("id") int id) {
        return itemService.getAllReviewsForItem(id);
    }
}
