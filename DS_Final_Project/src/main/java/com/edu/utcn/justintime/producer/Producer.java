package com.edu.utcn.justintime.producer;

import com.edu.utcn.justintime.entities.Item;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.lang.SerializationUtils;

/**
 * Created by Asus on 2/17/2017.
 */
public class Producer {

    private static final String EXCHANGE_NAME = "logs";

    public void sendItem(Item item) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        channel.basicPublish(EXCHANGE_NAME, "", null, SerializationUtils.serialize(item));
        System.out.println(" [x] Sent '" + item.toString() + "'");

        channel.close();
        connection.close();
    }
}
