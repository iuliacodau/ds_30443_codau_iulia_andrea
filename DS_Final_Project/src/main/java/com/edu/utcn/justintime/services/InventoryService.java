package com.edu.utcn.justintime.services;

import com.edu.utcn.justintime.dtos.InventoryDTO;
import com.edu.utcn.justintime.dtos.ItemDTO;
import com.edu.utcn.justintime.dtos.StoreDTO;
import com.edu.utcn.justintime.entities.Inventory;
import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Store;
import com.edu.utcn.justintime.entities.User;
import com.edu.utcn.justintime.errorhandlers.EntityValidationException;
import com.edu.utcn.justintime.repositories.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 2/12/2017.
 */

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    public int removeFromInventory(ItemDTO itemDTO, int amount) {
        List<String> validationErrors = new ArrayList<>();
        Item item = new Item(itemDTO);
        Inventory inventory = inventoryRepository.findByItem(item);
        if (inventory.getAmount() < amount) {
            validationErrors.add("Not enough products in inventory");
            throw new EntityValidationException(Inventory.class.getSimpleName(), validationErrors);
        }
        inventory.setAmount(inventory.getAmount() - amount);
        return inventoryRepository.save(inventory).getInventoryId();
    }

    public int addToInventory(ItemDTO itemDTO, int amount) {
        Item item = new Item(itemDTO);
        Inventory inventory = inventoryRepository.findByItem(item);
        inventory.setAmount(inventory.getAmount() + amount);
        return inventoryRepository.save(inventory).getInventoryId();
    }

    //do not really need this or?
    public List<InventoryDTO> findAll() {
        List<Inventory> inventories = inventoryRepository.findAll();
        List<InventoryDTO> toReturn = new ArrayList<InventoryDTO>();
        for (Inventory inventory : inventories) {
            InventoryDTO dto = new InventoryDTO.Builder()
                    .id(inventory.getInventoryId())
                    .amount(inventory.getAmount())
                    .item(new ItemDTO())
                    .store(new StoreDTO())
                    .create();

            toReturn.add(dto);
        }
        return toReturn;
    }
}
