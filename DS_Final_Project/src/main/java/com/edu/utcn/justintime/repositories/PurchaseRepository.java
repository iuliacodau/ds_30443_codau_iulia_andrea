package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.entities.Cart;
import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Purchase;
import com.edu.utcn.justintime.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Asus on 2/9/2017.
 */

@Transactional
public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {

    Purchase findByPurchaseId(Integer id);
    List<Purchase> findByItem(Item item);
    List<Purchase> findByCart(Cart cart);

}
