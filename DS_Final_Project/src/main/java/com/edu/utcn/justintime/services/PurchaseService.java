package com.edu.utcn.justintime.services;

import com.edu.utcn.justintime.dtos.*;
import com.edu.utcn.justintime.entities.*;
import com.edu.utcn.justintime.errorhandlers.EntityValidationException;
import com.edu.utcn.justintime.errorhandlers.ResourceNotFoundException;
import com.edu.utcn.justintime.repositories.CartRepository;
import com.edu.utcn.justintime.repositories.ItemRepository;
import com.edu.utcn.justintime.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Asus on 2/9/2017.
 */

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    CartRepository cartRepository;

    @Autowired
    ItemRepository itemRepository;

    public PurchaseDTO findById(int id) {
        Purchase purchase = purchaseRepository.findByPurchaseId(id);
        if (purchase == null) {
            throw new ResourceNotFoundException(Purchase.class.getSimpleName());
        }

        PurchaseDTO dto = new PurchaseDTO.Builder()
                .id(purchase.getPurchaseId())
                .amount(purchase.getAmount())
                .create();

        dto.setCart(purchase.getCart());
        dto.setItem(purchase.getItem());
        return dto;
    }

    public List<PurchaseDTO> findAll() {
        List<Purchase> purchases = purchaseRepository.findAll();
        List<PurchaseDTO> toReturn = new ArrayList<PurchaseDTO>();
        for (Purchase purchase : purchases) {
            PurchaseDTO dto = new PurchaseDTO.Builder()
                    .id(purchase.getPurchaseId())
                    .amount(purchase.getAmount())
                    .create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public int create(PurchaseDTO purchaseDTO) {
        List<String> validationErrors = validatePurchase(purchaseDTO);
        if (!validationErrors.isEmpty()) {
            throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
        }

        Purchase purchase = new Purchase();
        purchase.setItem(purchaseDTO.getItem());
        purchase.setAmount(purchaseDTO.getAmount());
        purchaseDTO.getCart().setTotal(purchaseDTO.getItem().getPrice() * purchaseDTO.getAmount());
        purchase.setCart(purchaseDTO.getCart());

        return purchaseRepository.save(purchase).getPurchaseId();
    }

    public int addNewPurchase(int cartId, int itemId, int amount) {
        Cart cart = cartRepository.findOne(cartId);
        Purchase purchase = new Purchase();
        Item item = itemRepository.findOne(itemId);
        purchase.setItem(item);
        purchase.setAmount(amount);

        Set<Purchase> purchases = cart.getPurchases();
        purchases.add(purchase);

        cart.setTotal(cart.getTotal() + (amount * item.getPrice()));

        cartRepository.save(cart);
        purchase.setCart(cart);
        return purchaseRepository.save(purchase).getPurchaseId();
    }

    private List<String> validatePurchase(PurchaseDTO purchase) {
        List<String> validationErrors = new ArrayList<String>();

        if (purchase.getAmount() <= 0) {
            validationErrors.add("Amount must be positive");
        }

        return validationErrors;
    }

    public List<PurchaseDTO> findByCartId(int id) {
        Cart cart = cartRepository.findOne(id);
        List<Purchase> purchases = purchaseRepository.findByCart(cart);
        List<PurchaseDTO> toReturn = new ArrayList<PurchaseDTO>();
        for (Purchase purchase : purchases) {

            PurchaseDTO dto = new PurchaseDTO.Builder()
                    .id(purchase.getPurchaseId())
                    .amount(purchase.getAmount())
                    .create();
            dto.setItem(purchase.getItem());
            dto.setCart(purchase.getCart());
            toReturn.add(dto);
        }
        return toReturn;
    }
}
