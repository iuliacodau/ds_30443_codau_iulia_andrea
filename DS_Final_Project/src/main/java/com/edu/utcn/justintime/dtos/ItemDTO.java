package com.edu.utcn.justintime.dtos;

import com.edu.utcn.justintime.entities.Inventory;
import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Purchase;
import com.edu.utcn.justintime.entities.Review;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Asus on 2/9/2017.
 */
public class ItemDTO {

    private Integer itemId;
    private String name;
    private int price;
    private String brand;
    private Set<PurchaseDTO> purchases = new HashSet<PurchaseDTO>(
            0);
    private Set<InventoryDTO> inventories = new HashSet<InventoryDTO>(
            0);
    private Set<ReviewDTO> reviews = new HashSet<ReviewDTO>(
            0);

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Set<PurchaseDTO> getPurchases() {
        return purchases;
    }

    public void setPurchases(Set<PurchaseDTO> purchases) {
        this.purchases = purchases;
    }

    public Set<InventoryDTO> getInventories() {
        return inventories;
    }

    public void setInventories(Set<InventoryDTO> inventories) {
        this.inventories = inventories;
    }

    public Set<ReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(Set<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

    public ItemDTO(){

    }

    public ItemDTO(Integer itemId, String name, int price, String brand, Set<PurchaseDTO> purchases, Set<InventoryDTO> inventories, Set<ReviewDTO> reviews) {
        super();
        this.itemId = itemId;
        this.name = name;
        this.price = price;
        this.brand = brand;
        this.purchases = purchases;
        this.inventories = inventories;
        this.reviews = reviews;
    }

    public static class Builder {
        private int nesteditemId;
        private String nestedname;
        private int nestedprice;
        private String nestedbrand;
        private Set<PurchaseDTO> nestedpurchases = new HashSet<>(0);
        private Set<InventoryDTO> nestedinventories = new HashSet<>(0);
        private Set<ReviewDTO> nestedreviews = new HashSet<>(0);


        public Builder id(int id) {
            this.nesteditemId = id;
            return this;
        }

        public Builder name(String name) {
            this.nestedname = name;
            return this;
        }

        public Builder price(int price) {
            this.nestedprice = price;
            return this;
        }

        public Builder brand(String brand) {
            this.nestedbrand = brand;
            return this;
        }

        public Builder purchases(Set<PurchaseDTO> purchases) {
            this.nestedpurchases = purchases;
            return this;
        }

        public Builder inventories(Set<InventoryDTO> inventories) {
            this.nestedinventories = inventories;
            return this;
        }

        public Builder reviews(Set<ReviewDTO> reviews) {
            this.nestedreviews = reviews;
            return this;
        }

        public ItemDTO create() {
            return new ItemDTO(nesteditemId,
                    nestedname,
                    nestedprice,
                    nestedbrand,
                    nestedpurchases,
                    nestedinventories,
                    nestedreviews);
        }
    }
}
