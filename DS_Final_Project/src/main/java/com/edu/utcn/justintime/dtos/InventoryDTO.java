package com.edu.utcn.justintime.dtos;

import com.edu.utcn.justintime.entities.Item;
import com.edu.utcn.justintime.entities.Store;

/**
 * Created by Asus on 2/9/2017.
 */
public class InventoryDTO {

    private Integer inventoryId;
    private ItemDTO item;
    private StoreDTO store;
    private int amount;

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public ItemDTO getItem() {
        return item;
    }

    public void setItem(ItemDTO item) {
        this.item = item;
    }

    public StoreDTO getStore() {
        return store;
    }

    public void setStore(StoreDTO store) {
        this.store = store;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public InventoryDTO(){}

    public InventoryDTO(Integer inventoryId, ItemDTO item, StoreDTO store, int amount) {
        this.inventoryId = inventoryId;
        this.item = item;
        this.store = store;
        this.amount = amount;
    }

    public static class Builder{

        private Integer nestedinventoryId;
        private ItemDTO nesteditem;
        private StoreDTO nestedstore;
        private int nestedamount;

        public Builder id(Integer id) {
            this.nestedinventoryId = id;
            return this;
        }

        public Builder item(ItemDTO item) {
            this.nesteditem = item;
            return this;
        }

        public Builder store(StoreDTO store) {
            this.nestedstore = store;
            return this;
        }

        public Builder amount(int amount) {
            this.nestedamount = amount;
            return this;
        }

        public InventoryDTO create() {
            return new InventoryDTO(
                    nestedinventoryId,
                    nesteditem,
                    nestedstore,
                    nestedamount);
        }

    }
}
