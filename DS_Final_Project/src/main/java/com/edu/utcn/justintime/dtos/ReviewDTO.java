package com.edu.utcn.justintime.dtos;

import com.edu.utcn.justintime.entities.Item;

/**
 * Created by Asus on 2/9/2017.
 */
public class ReviewDTO {

    private Integer reviewId;
    private Item item;
    private int rating;
    private byte approved;
    private String comment;

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public byte getApproved() {
        return approved;
    }

    public void setApproved(byte approved) {
        this.approved = approved;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ReviewDTO(){

    }

    public ReviewDTO(Integer reviewId, Item item, int rating, byte approved, String comment) {
        super();
        this.reviewId = reviewId;
        this.item = item;
        this.rating = rating;
        this.approved = approved;
        this.comment = comment;
    }

    public static class Builder{

        private Integer nestedreviewId;
        private Item nesteditem;
        private int nestedrating;
        private byte nestedapproved;
        private String nestedcomment;

        public Builder id(Integer id) {
            this.nestedreviewId = id;
            return this;
        }

        public Builder item(Item item) {
            this.nesteditem = item;
            return this;
        }

        public Builder rating(int rating) {
            this.nestedrating = rating;
            return this;
        }

        public Builder nestedaproved(byte approved) {
            this.nestedapproved = approved;
            return this;
        }

        public Builder comment(String comment) {
            this.nestedcomment = comment;
            return this;
        }

    }
}
