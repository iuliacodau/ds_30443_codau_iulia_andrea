package com.edu.utcn.justintime.repositories;

import com.edu.utcn.justintime.dtos.UserDTO;
import com.edu.utcn.justintime.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Asus on 2/10/2017.
 */

@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUserId(Integer id);
    User findByUsername(String username);


}